<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;
use LogLib\LogLib;
use Fuel\Core\Package;

/**
 * Import news feeds from Rss
 * 
 * @package             Tasks
 * @create              Dec 29 2014
 * @version             1.0
 * @author             <Tuancd>
 * @run                 php oil refine PushMessage
 * @copyright           Oceanize INC
 */
class PushMessage {

    public static function run() {
        Cli::write("Begin Processing . . . . ! \n");
        try {
            // Get list Push Message WHERE send_reservation_date = NOW() AND is_sent = 0 AND disable = 0 limit by còn
            $push_message = \Model_Push_Message::get_list_push_message_for_task();
            if (!empty($push_message)) {
                // Start Block
                \LogLib::info('Begin Block send message for device with push_message_id: ', __METHOD__, $push_message['id']);
                $message = $push_message['message'];

                // Get list user limit to send message
                $param = array(
                    'limit' => Config::get('limit_user_push_message', true),
                    'message_id' => $push_message['id']
                );
                $lstUser = \Model_User_Notification::get_list_user_push_message($param);

                if (!empty($lstUser)) {
                    $count = 0;
                    foreach ($lstUser as $item) {
                        try {
                            $google_regid = $item['google_regid'];
                            $apple_regid = $item['apple_regid'];
                            if (!empty($google_regid)) {
                                // Push message to Android
                                \Gcm::sendMessage(array(
                                    'google_regid' => $google_regid,
                                    'message' => $message
                                ));
                                $count ++;
                                Cli::write("\t-Send message: [$message] for Android with ID: $google_regid  \n");
                                \LogLib::info('End Block send message for device', __METHOD__, array(
                                    'google_regid' => $google_regid,
                                    'user_id' => $item['user_id']
                                ));
                                // Write push_message_send_log
                                \Model_Push_Message_Send_Log::add(array(
                                    'user_id' => $item['user_id'],
                                    'message_id' => $push_message['id']
                                ));
                            }

                            if (!empty($apple_regid)) {
                                // Push message to Android
                                \Apns::sendMessage(array(
                                    'apple_regid' => $apple_regid,
                                    'message' => $message,
                                    'ios_sound' => $push_message['ios_sound']
                                ));
                                $count ++;
                                Cli::write("\t-Send message: [$message] for Apple with ID: $apple_regid  \n");
                                \LogLib::info('End Block send message for device', __METHOD__, array(
                                    'apple_regid' => $apple_regid,
                                    'user_id' => $item['user_id']
                                ));
                                // Write push_message_send_log
                                \Model_Push_Message_Send_Log::add(array(
                                    'user_id' => $item['user_id'],
                                    'message_id' => $push_message['id']
                                ));
                            }
                        } catch (Exception $exc) {
                            \LogLib::error('Error for send message for user', __METHOD__, $item);
                        }
                    }
                    Cli::write("Successfull sending message for $count user  \n");
                } else {
                    Cli::write("Begin Update flag is_sent for Push_message id: " . $push_message['id'] . "  \n");
                    // If has no user to send => update flag is_sen = 1 for push_message            
                    \Model_Push_Message::add_update(array(
                        'id' => $push_message['id'],
                        'is_sent' => 1, 
                        'sent_date' => time()
                    ));
                    Cli::write("Update Successfull flag is_sent for Push_message id: " . $push_message['id'] . "  \n");
                }
                // End Block
                \LogLib::info('End Block send message for device with push_message_id: ', __METHOD__, $push_message['id']);
            } else {
                \LogLib::info('Has no message to push!!', __METHOD__, null);
                Cli::write("- Has no message to push!!  \n");
            }
            Cli::write("Processing Done . . . . ! \n");
        } catch (Exception $exc) {

            \LogLib::error('Has error while processing.', __METHOD__, $item);
            Cli::write("Has error while processing.");
        }
    }

}

