<?php

/**
 * Any query for model prefecture
 *
 * @package Model
 * @created 2015-02-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Prefecture extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'name',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'prefectures';

    /**
     * Get list prefecture
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list prefecture
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name);

        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }

        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Update info for prefecture
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns prefecture id or false if error
     */
    public static function set_update($param)
    {
        $prefecture = self::find($param['id']);
        if (empty($prefecture)) {
            static::errorNotExist('prefecture_id', $param['id']);
            return false;
        }

        if (!empty($param['name'])) {
            $prefecture->set('name', $param['name']);
        }

        if ($prefecture->update()) {
            if (empty($prefecture->id)) {
                $prefecture->id = self::cached_object($prefecture)->_original['id'];
            }
            return !empty($prefecture->id) ? $prefecture->id : 0;
        }
        return false;
    }

    /**
     * Get all prefecture
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns array all prefecture
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name . '.id',
            self::$_table_name . '.name'
        )
            ->from(self::$_table_name);
        $query->order_by(self::$_table_name . '.id', 'ASC');

        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }
}
