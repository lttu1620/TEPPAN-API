<?php

/**
 * Any query in Model Campus
 *
 * @package Model
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Campus extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'university_id',
		'name',
		'address',
		'access_method',
		'prefecture_id',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'campuses';

    /**
     * Get list campuses by university_id
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list campuses
     */
    public static function get_list($param)
    {
        $query = DB::select(
            array('universities.name', 'university_name'),
            self::$_table_name . '.*'
        )
            ->from('universities')
            ->join(self::$_table_name, 'LEFT')
            ->on('universities.id', '=', self::$_table_name . '.university_id');
        if (!empty($param['university_id'])) {
            $query->where('university_id', '=', $param['university_id']);
        }
        if (!empty($param['name'])) {
            $query->where('campuses.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['address'])) {
            $query->where('address', 'LIKE', "%{$param['address']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        if (!isset($param['page'])) {
            return $data;
        }
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);  
    }

    /**
     * enable/disable a or any campuses
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $campus = self::find($id);
            $campus->set('disable', $param['disable']);
            if (!$campus->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Add or update info for campuses
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns campus id or false if error
     */
    public static function add_update($param)
    {
        try {
            $id = !empty($param['id']) ? $param['id'] : 0;
            $campus = new self;
            if (!empty($id)) {
                $campus = self::find($id);
                if (empty($campus)) {
                    return false;
                }
            }
            if (!empty($param['university_id'])) {
                $campus->set('university_id', $param['university_id']);
            }
            if (!empty($param['name'])) {
                $campus->set('name', $param['name']);
            }
            if (!empty($param['address'])) {
                $campus->set('address', $param['address']);
            }
            if (!empty($param['access_method'])) {
                $campus->set('access_method', $param['access_method']);
            }
            if (!empty($param['prefecture_id'])) {
                $campus->set('prefecture_id', $param['prefecture_id']);
            }
            if (isset($param['disable']) && $param['disable'] != '') {
                $campus->set('disable', $param['disable']);
            }
            if ($campus->save()) {
                if (empty($campus->id)) {
                    $campus->id = self::cached_object($campus)->_original['id'];
                }
                return !empty($campus->id) ? $campus->id : 0;
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}

