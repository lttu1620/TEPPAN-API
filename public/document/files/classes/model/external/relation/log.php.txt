<?php

/**
 * Any query for model external relation log
 *
 * @package Model
 * @created 2015-01-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_External_Relation_Log extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'called_api_name',
		'apply_app_id',
		'called_from',
		'disable',
		'created',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'          => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'          => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'external_relation_logs';

	/**
	 * Add log after calling API
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return int|bool Returns log id or false if error
	 */
	public static function insert($param)
	{
		$log = new self;
		$log->set('called_api_name', $param['called_api_name']);
		$log->set('apply_app_id', $param['apply_app_id']);
		$log->set('called_from', $param['os']);
		if ($log->create()) {
			return self::cached_object($log)->_original['id'];
		}

		return false;
	}
}
