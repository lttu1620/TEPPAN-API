<?php

/**
 * Any query for model user mail Logs
 *
 * @package Model
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Mail_Log extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'user_id',
		'template_id',
		'title',
		'content',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'          => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'          => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'user_mail_logs';

	/**
	 * Get list user mail logs
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return array Returns list user mail logs
	 */
	public static function get_list($param)
	{
		$query = DB::select(
			self::$_table_name . '.*',
			array('users.name', 'user_name'),
			array('templates.title', 'template_title')
		)
			->from(self::$_table_name)
			->join('users', 'LEFT')
				->on(self::$_table_name . '.user_id', '=', 'users.id')
			->join('templates', 'LEFT')
				->on(self::$_table_name . '.template_id', '=', 'templates.id');

		if (!empty($param['title'])) {
			$query->where(self::$_table_name . '.title', 'LIKE', "%{$param['title']}%");
		}
		if (!empty($param['name'])) {
			$query->where('users.name', 'LIKE', "%{$param['name']}%");
		}
		if (!empty($param['mail'])) {
			$query->where('users.mail', 'LIKE', "%{$param['mail']}%");
		}
		if (!empty($param['template_title'])) {
			$query->where('templates.title', 'LIKE', "%{$param['template_title']}%")
				->or_where('templates.title_en', 'LIKE', "%{$param['template_title']}%")
				->or_where('templates.title_cn', 'LIKE', "%{$param['template_title']}%");
		}
		if (!empty($param['sort'])) {
			$sortExplode = explode('-', $param['sort']);
			if ($sortExplode[0] == 'created') {
				$sortExplode[0] = self::$_table_name . '.created';
			}
			$query->order_by($sortExplode[0], $sortExplode[1]);
		} else {
			$query->order_by(self::$_table_name . '.created', 'DESC');
		}
		if (!empty($param['page']) && !empty($param['limit'])) {
			$offset = ($param['page'] - 1) * $param['limit'];
			$query->limit($param['limit'])->offset($offset);
		}
		$data = $query->execute()->as_array();
		$total = !empty($data) ? DB::count_last_query() : 0;

		return array($total, $data);
	}

	/**
	 * Add info for user mail logs
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return int|bool Returns log id or false if error
	 */
	public static function add($param)
	{
		try {
			$log = new self;
			$log->set('user_id', $param['user_id']);
			$log->set('template_id', $param['template_id']);
			$log->set('title', $param['title']);
			$log->set('content', $param['content']);
			if ($log->create()) {
				$log->id = self::cached_object($log)->_original['id'];
				return !empty($log->id) ? $log->id : 0;
			}

			return false;
		} catch (\Exception $e) {
			\LogLib::error($e->getMessage());
			return false;
		}
	}
}

