<?php

namespace Bus;

/**
 * Users_Forget password - API to process forget password Users
 *
 * @package Bus
 * @created 2014-12-18
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Users_Forgetpassword extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'email',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /**
     * Call function forget_password() from Model User
     *
     * @author tuancd
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_User::forget_password($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

