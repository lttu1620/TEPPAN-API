<?php

namespace Bus;

/**
 * Enable/Disable Signature
 *
 * @package Bus
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Signatures_Disable extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'       => array(1, 11),
        'language_type' => 1,
        'disable'       => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'language_type',
        'disable'
    );

    /**
     * Call function disable() from model Signatures
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Signature::disable($data);
            return $this->result(\Model_Signature::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

