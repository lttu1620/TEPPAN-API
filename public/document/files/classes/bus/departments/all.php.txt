<?php

namespace Bus;

/**
 * Get list Departments
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Departments_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'university_id'
    );

    /**
     * Call function get_all() from model department
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $options['where'][] = array(
                'disable' => 0
            );
            if (!empty($param['university_id'])) {
                $options['where'][] = array(
                    'university_id' => $param['university_id']
                );
            }
            $this->_response = \Model_Department::find('all', $options);
            return $this->result(\Model_Department::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

