<?php

namespace Bus;

/**
 * Add info for User Template
 *
 * @package   API
 * @created   2015-02-12
 * @version   1.0
 * @author    Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserTemplates_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'template_id',
        'title',
        'content'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'          => array(1, 11),
        'business_card_id' => array(1, 11),
        'template_id'      => array(1, 11),
        'title'            => array(0, 128)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'business_card_id',
        'template_id'
    );

    /**
     * Call function add() from model User Template
     *
     * @author  Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Template::add($data);
            return $this->result(\Model_User_Template::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

