<?php

namespace Bus;

/**
 * Get all help content
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class HelpContents_All extends BusAbstract
{
    /**
     * Call function get_all() from model Help Content
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Help_Content::get_all($data);
            return $this->result(\Model_Help_Content::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

