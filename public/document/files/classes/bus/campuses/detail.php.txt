<?php

namespace Bus;

/**
 * Get detail campus
 *
 * @package Bus
 * @created 2014-12-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Campuses_Detail extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Get detail campus by id
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campuse::get_detail($data);//find($data['id']);
            return $this->result(\Model_Campus::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

