<?php

namespace Bus;

/**
 * Add or update info for campus
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Campuses_AddUpdate extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'university_id',
        'prefecture_id'
    );

    /**
     * Call function add_update() from model Campus
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campus::add_update($data);
            return $this->result(\Model_Campus::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

