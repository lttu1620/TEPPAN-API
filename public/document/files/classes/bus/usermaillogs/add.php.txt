<?php

namespace Bus;

/**
 * Add info for user mail logs
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserMailLogs_Add extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'user_id',
		'template_id',
		'title',
		'content'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'user_id'     => array(1, 11),
		'template_id' => array(1, 11),
		'title'       => array(0, 255)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'user_id',
		'template_id'
	);

	/**
	 * Call function add() from model user mail Logs
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_User_Mail_Log::add($data);
			return $this->result(\Model_User_Mail_Log::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}

