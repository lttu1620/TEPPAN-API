<?php

/**
 * Controller for actions on User Schools
 *
 * @package Controller
 * @created 2015-03-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserSchools extends \Controller_App
{
    /**
     * Add user school
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_add()
    {
        return \Bus\UserSchools_Add::getInstance()->execute();
    }

    /**
     * Update user school
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_update()
    {
        return \Bus\UserSchools_Update::getInstance()->execute();
    }

    /**
     * Get user school info to edit
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_edit()
    {
        return \Bus\UserSchools_Edit::getInstance()->execute();
    }

    /**
     * Get detail user school
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\UserSchools_Detail::getInstance()->execute();
    }
}
