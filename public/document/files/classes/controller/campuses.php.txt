<?php

/**
 * Controller for actions on Campuses
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Campuses extends \Controller_App
{
    /**
     * Get all campus
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\Campuses_All::getInstance()->execute();
    }

    /**
     * Get detail campus
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\Campuses_Detail::getInstance()->execute();
    }

    /**
     * Get list campus
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Campuses_List::getInstance()->execute();
    }

    /**
     * Disable campus
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\Campuses_Disable::getInstance()->execute();
    }

    /**
     * Add, update campus
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\Campuses_AddUpdate::getInstance()->execute();
    }

}
