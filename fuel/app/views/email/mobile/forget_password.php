※このメールにお心当たりのない場合は、URLにアクセスせずメールを破棄してください。<br />
<br />
いつもCaptureをご利用いただきありがとうございます。<br />
<br />
パスワードの再設定を行います。<br />
アプリ内の画面で以下の再設定コードを入力してパスワードを再設定してください。<br />
<br />
【再設定コード：】<br />
<?php echo $keycode; ?><br />
<br />

※再設定コードの有効期限は24時間です。有効期限を過ぎた場合には再度再設定コードを発行してください。	
