<?php

use Fuel\Core\DB;

/**
 * Any query in Model University
 *
 * @package Model
 * @created 2014-11-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_University extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'name',
        'kana',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'universities';

    /**
     * Get list university by name, kana
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list university
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['kana'])) {
            $query->where('kana', 'LIKE', "%{$param['kana']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('disable', '=', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        if (!isset($param['page'])) {
            return $data;
        }
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Enable/disable a or any university
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $university = self::find($id);
            $university->set('disable', $param['disable']);
            if (!$university->save()) {
                return false;
            }
        }
        self::delete_cache();
        return true;
    }

    /**
     * Add or update info for university
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns university id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $university = new self;
        if (!empty($id)) {
            $university = self::find($id);
            if (empty($university)) {
                return false;
            }
        }
        if (!empty($param['name'])) {
            $university->set('name', $param['name']);
        }
        if (!empty($param['kana'])) {
            $university->set('kana', $param['kana']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $university->set('disable', $param['disable']);
        }
        if ($university->save()) {
            if (empty($university->id)) {
                $university->id = self::cached_object($university)->_original['id'];
            }
            self::delete_cache();
            return !empty($university->id) ? $university->id : 0;
        }
        return false;
    }

    /**
     * Get all university
     *
     * @author thailh
     * @param array $param
     * @return array Returns array all university
     */
    public static function all($param = array())
    {
        $key = 'university_all';
        $data = \Lib\Cache::get($key);
        if ($data === false) {
            $options['where'][] = array(
                'disable' => 0
            );
            $options['select'] = array(
                'id', 'name', 'kana'
            );
            $data = self::find('all', $options);
            \Lib\Cache::set($key, $data);
        }
        return $data;
    }

    /**
     * Delete cache
     *
     * @author Le Tuan Tu
     * @param array $param
     * @return bool Returns result of action
     */
    public static function delete_cache($param = array())
    {
        return \Lib\Cache::delete('university_all');
    }
}
