<?php

/**
 * Any query in Model Template Favorite
 *
 * @package Model
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Template_Favorite extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'template_id',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'template_favorites';

    /**
     * Get list user of template favorite by template id
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list user of template favorite
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . ".*",
            array('users.name', 'username'),
            array('users.mail', 'mail'),
            array('users.image', 'image'),
            'templates.title'
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . ".user_id", '=', 'users.id')
            ->join('templates')
            ->on(self::$_table_name . ".template_id", '=', 'templates.id');

        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['mail'])) {
            $query->where('users.email', '=', "{$param['mail']}");
        }
        if (!empty($param['template_id'])) {
            $query->where(self::$_table_name . '.template_id', '=', "{$param['template_id']}");
        }
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', "{$param['user_id']}");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        //$query->group_by(self::$_table_name . '.user_id');
        $data  = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Enable/disable a or any template favorites
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'][] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'][] = array(
                'template_id' => $param['template_id'],
                'user_id'     => $param['user_id'],
                'disable'     => '0'
            );
        }
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                return true;
            }
            return false;
        } else {
            if (!empty($param['id'])) {
                static::errorNotExist('id', $param['id']);
            } else {
                static::errorNotExist('template_id', $param['template_id']);
            }
            return false;
        }
    }

    /**
     * Add info for template favorites
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns template favorite id or false if error
     */
    public static function add($param)
    {
        $query = DB::select(
            array('templates.id', 'template_id'),
            array('template_favorites.id', 'favorites_id'),
            array('template_favorites.user_id', 'user_id'),
            array('template_favorites.disable', 'favorite_disable')
        )
            ->from('templates')
            ->join(
                DB::expr(
                    "(SELECT * FROM template_favorites WHERE user_id={$param['user_id']}) AS template_favorites"
                ),
                'LEFT'
            )
            ->on('templates.id', '=', 'template_favorites.template_id')
            ->where('templates.id', '=', $param['template_id'])
            ->where('templates.disable', '=', '0')
            ->limit(1)
            ->offset(0);
        $data  = $query->execute()->as_array();
        $data  = !empty($data[0]) ? $data[0] : array();
        if (empty($data['template_id'])) {
            static::errorNotExist('template_id', $param['template_id']);
            return false;
        }
        if (!empty($data['user_id']) && $data['favorite_disable'] == 0) {
            static::errorDuplicate('user_id', $param['user_id']);
            return false;
        }
        $new = false;
        if (!empty($data['user_id']) && $data['favorite_disable'] == 1) {
            $dataUpdate = array(
                'id'      => $data['favorites_id'],
                'disable' => '0'
            );
        } else {
            $new        = true;
            $dataUpdate = array(
                'template_id' => $data['template_id'],
                'user_id'     => $param['user_id']
            );
        }
        $favorites = new self($dataUpdate, $new);
        if ($favorites->save()) {
            if ($new == true) {
                $favorites->id = self::cached_object($favorites)->_original['id'];
            }
            return $favorites->id;
        }
        return false;
    }

    /**
     * Get all Template Favorite
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns array all Template Favorite
     */
    public static function get_all($param)
    {
        $param['user_id'] = !empty($param['user_id']) ? $param['user_id'] : 0;
        $query            = DB::select(
            array(self::$_table_name . '.template_id', 'id'),
            'templates.mail_type_id',
            'templates.mail_situation_id',
            'templates.title',
            'templates.subject',
            'templates.content',
            'templates.memo',
            'templates.language_type',
            'templates.is_public',
            'templates.made_by',
            'templates.favorite_count',
            'templates.disable',
            'templates.created',
            'templates.updated',
            DB::expr("
				(CASE language_type
					WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_types.name
					WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_types.name_en
					WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_types.name_cn
				END) as mail_type_name
			"),
            DB::expr("
				(CASE language_type
					WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_situations.name
					WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_situations.name_en
					WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_situations.name_cn
				END) as mail_situation_name
			"),
            array('users.name', 'username_made_by'),
            DB::expr("
				IF (ISNULL(favorites.id), 0, 1) AS is_favorite
			")
        )
            ->from(self::$_table_name)
            ->join('templates')
            ->on(self::$_table_name . '.template_id', '=', 'templates.id')
            ->join('mail_types')
            ->on('templates.mail_type_id', '=', 'mail_types.id')
            ->join('mail_situations')
            ->on('templates.mail_situation_id', '=', 'mail_situations.id')
            ->join('users', 'LEFT')
            ->on('templates.made_by', '=', 'users.id')
            ->join(DB::expr("(
				SELECT *
				FROM   template_favorites
				WHERE  disable = 0
					   AND user_id = {$param['user_id']}
			) favorites"),
                'LEFT')
            ->on('templates.id', '=', 'favorites.template_id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('templates.disable', '=', '0');

        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (!empty($param['template_id'])) {
            $query->where(self::$_table_name . '.template_id', '=', $param['template_id']);
        }
        $query->order_by(self::$_table_name . '.created', 'DESC');
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $query->group_by(self::$_table_name . '.template_id');
        $data = $query->execute()->as_array();
        if ($data) {
            $signature = Model_Signature::get_detail(array(
                'user_id'       => $param['user_id'],
                'language_type' => $param['language_type'],
            ));
            $content   = $signature ? $signature['content'] : \Config::get(\Config::get('default_signature_setting')[$param['language_type']]);
            foreach ($data as &$item) {
                $item['content'] .= $content;
            }
        }
        return $data;
    }
    
    /**
     * Get for favorites report.
     *
     * @author thailh
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_for_report($param)
    {
        $query = DB::select(
                'templates.id',
                'templates.title',
                DB::expr("
                    COUNT(" . self::$_table_name . ".user_id) AS count_favorite
                ")
            )
            ->from(self::$_table_name)
            ->join('templates')
            ->on(self::$_table_name . '.template_id', '=', 'templates.id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('templates.disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.updated', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.updated', '<=', self::date_to_val($param['date_to']));
        }
        $query->group_by(self::$_table_name . '.template_id');
        $query->order_by('count_favorite', 'DESC');
        $query->limit(10)->offset(0);
        $data = $query->execute()->as_array();
        return $data;
    }
    
}
