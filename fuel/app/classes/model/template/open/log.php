<?php

/**
 * Any query for model Template Open Log
 *
 * @package Model
 * @created 2015-02-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Template_Open_Log extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'user_id',
		'template_id',
		'disable',
		'created'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'          => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'          => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'template_open_logs';

	/**
	 * Get list Template Open logs
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return array Returns list Template Open logs
	 */
	public static function get_list($param)
	{
		$query = DB::select(
			self::$_table_name . '.*',
			array('users.name', 'username'),
			array('templates.title', 'template_title'),
                        array('user_profiles.email', 'email')
		)
			->from(self::$_table_name)
			->join('users', 'LEFT')
			->on(self::$_table_name . '.user_id', '=', 'users.id')
			->join('templates', 'LEFT')
			->on(self::$_table_name . '.template_id', '=', 'templates.id')
                        ->join('user_profiles', 'LEFT')
			->on(self::$_table_name . '.user_id', '=', 'user_profiles.user_id');

		if (!empty($param['user_id'])) {
			$query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
		}
		if (!empty($param['template_id'])) {
			$query->where(self::$_table_name . '.template_id', '=', $param['template_id']);
		}
		if (!empty($param['name'])) {
			$query->where('users.name', 'LIKE', "%{$param['name']}%");
		}
                if (!empty($param['email'])) {
			$query->where('user_profiles.email', '=', $param['email']);
		}
		if (!empty($param['date_from'])) {
			$query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
		}
		if (!empty($param['date_to'])) {
			$query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
		}
		if (!empty($param['sort'])) {
			$sortExplode = explode('-', $param['sort']);
			if ($sortExplode[0] == 'created') {
				$sortExplode[0] = self::$_table_name . '.created';
			}
			$query->order_by($sortExplode[0], $sortExplode[1]);
		} else {
			$query->order_by(self::$_table_name . '.created', 'DESC');
		}
		if (!empty($param['page']) && !empty($param['limit'])) {
			$offset = ($param['page'] - 1) * $param['limit'];
			$query->limit($param['limit'])->offset($offset);
		}
		$data = $query->execute()->as_array();
		$total = !empty($data) ? DB::count_last_query() : 0;

		return array($total, $data);
	}

	/**
	 * Add info for Template Open logs
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return int|bool Returns log id or false if error
	 */
	public static function add($param)
	{
		$log = new self;
		$log->set('user_id', $param['user_id']);
		$log->set('template_id', $param['template_id']);
		if ($log->create()) {
			$log->id = self::cached_object($log)->_original['id'];
			return !empty($log->id) ? $log->id : 0;
		}
		return false;
	}

	/**
	 * Get list Template Open logs usually use
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return array Returns list Template Open logs usually use
	 */
	public static function get_usually($param)
	{
		$param['user_id'] = !empty($param['user_id']) ? $param['user_id'] : 0;
		$query = DB::select(
			array(self::$_table_name . '.template_id', 'id'),
			'templates.mail_type_id',
			'templates.mail_situation_id',
			'templates.title',
			'templates.subject',
			'templates.content',
			'templates.memo',
			'templates.language_type',
			'templates.is_public',
			'templates.made_by',
			'templates.favorite_count',
			'templates.disable',
			'templates.created',
			'templates.updated',
			DB::expr("
				(CASE language_type
					WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_types.name
					WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_types.name_en
					WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_types.name_cn
				END) as mail_type_name
			"),
			DB::expr("
				(CASE language_type
					WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_situations.name
					WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_situations.name_en
					WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_situations.name_cn
				END) as mail_situation_name
			"),
			array('users.name', 'username_made_by'),
			DB::expr("
				IF (ISNULL(favorites.id), 0, 1) AS is_favorite
			"),
			DB::expr("
				MAX(template_open_logs.created) AS last_using
			"),
			DB::expr("
				COUNT(template_open_logs.template_id) AS using_count
			")
		)
			->from(self::$_table_name)
			->join('templates')
			->on(self::$_table_name . '.template_id', '=', 'templates.id')
			->join('mail_types')
			->on('templates.mail_type_id', '=', 'mail_types.id')
			->join('mail_situations')
			->on('templates.mail_situation_id', '=', 'mail_situations.id')
			->join('users', 'LEFT')
			->on('templates.made_by', '=', 'users.id')
			->join(DB::expr("(
				SELECT *
				FROM   template_favorites
				WHERE  disable = 0
					   AND user_id = {$param['user_id']}
			) favorites"), 'LEFT')
			->on('templates.id', '=', 'favorites.template_id')
			->where(self::$_table_name . '.disable', '=', '0')
			->where('templates.disable', '=', '0');

		if (!empty($param['user_id'])) {
			$query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
		}
		if (!empty($param['template_id'])) {
			$query->where(self::$_table_name . '.template_id', '=', $param['template_id']);
		}
		if (!empty($param['date_from'])) {
			$query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
		} else {
			$query->where(self::$_table_name . '.created', '>=', self::date_from_val(date('Y-m-d', time() - 30*24*60*60)));
		}
		if (!empty($param['date_to'])) {
			$query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
		} else {
			$query->where(self::$_table_name . '.created', '<=', self::date_to_val(date('Y-m-d', time())));
		}
		$query->order_by('using_count', 'DESC');
		if (!empty($param['page']) && !empty($param['limit'])) {
			$offset = ($param['page'] - 1) * $param['limit'];
			$query->limit($param['limit'])->offset($offset);
		}
		$query->group_by(self::$_table_name . '.template_id');
		$data = $query->execute()->as_array();
		if ($data) {
			$signature = Model_Signature::get_detail(array(
				'user_id'       => $param['user_id'],
				'language_type' => $param['language_type'],
			));
			$content = $signature ? $signature['content'] : \Config::get(\Config::get('default_signature_setting')[$param['language_type']]);
			foreach ($data as &$item) {
				$item['content'] .= $content;
			}
		}
		return $data;
	}
    
    /**
     * Get for used templates report.
     *
     * @author thailh
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_for_report($param)
    {
        $query = DB::select(
                'templates.id',
                'templates.title',
                DB::expr("
                    COUNT(" . self::$_table_name . ".user_id) AS count_used
                ")
            )
            ->from(self::$_table_name)
            ->join('templates')
            ->on(self::$_table_name . '.template_id', '=', 'templates.id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('templates.disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $query->group_by(self::$_table_name . '.template_id');
        $query->order_by('count_used', 'DESC');
        $query->limit(10)->offset(0);
        $data = $query->execute()->as_array();
        return $data;
    }
    
}
