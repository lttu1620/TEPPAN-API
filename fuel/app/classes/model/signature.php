<?php

/**
 * Any query for model Signature
 *
 * @package Model
 * @created 2015-02-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Signature extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'language_type',
        'content',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'signatures';

    /**
     * Get list Signature by user id
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list Signature
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            DB::expr("
                (CASE language_type
					WHEN '" . \Config::get('language_type')['jp'] . "' THEN '" . \Config::get('language_name')[\Config::get('language_type')['jp']] . "'
					WHEN '" . \Config::get('language_type')['en'] . "' THEN '" . \Config::get('language_name')[\Config::get('language_type')['en']] . "'
					WHEN '" . \Config::get('language_type')['cn'] . "' THEN '" . \Config::get('language_name')[\Config::get('language_type')['cn']] . "'
				END) as language_name
            "),
            array('users.name', 'username')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        $query->order_by(self::$_table_name . '.language_type', 'ASC')
            ->order_by(self::$_table_name . '.user_id', 'ASC');
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Get detail of Signature
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns detail of Signature
     */
    public static function get_detail($param)
    {
        $param['language_type'] = !empty($param['language_type']) ? $param['language_type'] : 1;
        $query = DB::select(
            self::$_table_name . '.id',
            self::$_table_name . '.user_id',
            DB::expr("
                IFNULL(
                IF(signatures.content='',NULL,signatures.content),'" . \Config::get(\Config::get('default_signature_setting')[$param['language_type']]) . "')
                AS
                  content
            "),
            self::$_table_name . '.disable',
            self::$_table_name . '.created',
            self::$_table_name . '.updated',
            array('users.name', 'username')
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->where(self::$_table_name . '.disable', '=' . '0');
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', '=', $param['id']);
        } else {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id'])
                ->where(self::$_table_name . '.language_type', '=', $param['language_type']);
        }
        $data = $query->execute()->as_array();

        return $data ? $data[0] : array();
    }

    /**
     * Add and update info for Signature
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns signature id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        if (!empty($id)) { // update
            $template = self::find($id);
            if (empty($template)) {
                self::errorNotExist('signature_id', $id);
                return false;
            }
        } else { // add new record
            $options['where'] = array(
                'user_id'       => $param['user_id'],
                'language_type' => $param['language_type']
            );

            $template = self::find('first', $options);
            if (!empty($template)) {
                if ($template->get('disable') != '0') { // enable if record disabled
                    $template->set('disable', '0');
                }
            } else {
                $template = new self;
            }
        }

        if (!empty($param['user_id'])) {
            $template->set('user_id', $param['user_id']);
        }
        if (!empty($param['language_type'])) {
            $template->set('language_type', $param['language_type']);
        }
        if (!empty($param['content'])) {
            $template->set('content', $param['content']);
        }

        if ($template->save()) {
            if (empty($template->id)) {
                $template->id = self::cached_object($template)->_original['id'];
            }
            return !empty($template->id) ? $template->id : 0;
        }
        return false;
    }

    /**
     * Disable/enable Signatures
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) { // disable in admin
            $ids = explode(',', $param['id']);
            foreach ($ids as $id) {
                $template = self::find($id);
                if ($template) {
                    $template->set('disable', $param['disable']);
                    if (!$template->update()) {
                        return false;
                    }
                } else {
                    static::errorNotExist('signature_id', $id);
                    return false;
                }
            }
        } else { // disable in mobile
            $options['where'][] = array(
                'language_type' => $param['language_type'],
                'user_id'       => $param['user_id'],
                'disable'       => '0'
            );

            $template = self::find('first', $options);
            if ($template) {
                $template->set('disable', $param['disable']);
                if (!$template->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('signature_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get all Signatures
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns array all Signatures
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name . '.id',
            self::$_table_name . '.user_id',
            DB::expr("
                IFNULL(
                IF(signatures.content='',NULL,signatures.content),'" . \Config::get(\Config::get('default_signature_setting')[$param['language_type']]) . "')
                AS
                  content
            "),
            self::$_table_name . '.disable',
            self::$_table_name . '.created',
            self::$_table_name . '.updated'
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.disable', '=', '0');
        $query->order_by(self::$_table_name . '.id', 'ASC');

        $data = $query->execute()->as_array();

        return $data;
    }
}
