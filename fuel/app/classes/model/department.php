<?php

use Fuel\Core\DB;

/**
 * Any query in Model Department
 *
 * @package Model
 * @created 2014-11-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Department extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'university_id',
        'name',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'departments';

    /**
     * Get list department by university id, name
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list department
     */
    public static function get_list($param)
    {
        $query = DB::select(
            array('universities.name', ' university_name'),
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name)
            ->join('universities', 'LEFT')
            ->on(self::$_table_name . '.university_id', '=', 'universities.id');
        if (!empty($param['university_id'])) {
            $query->where('university_id', '=', $param['university_id']);
        }
        if (!empty($param['name'])) {
            $query->where('departments.name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('departments.disable', '=', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        if (!isset($param['page'])) {
            return $data;
        }
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Enable/disable a or any department
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $department = self::find($id);
            $department->set('disable', $param['disable']);
            if (!$department->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Add or update info for department
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns department id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $department = new self;
        if (!empty($id)) {
            $department = self::find($id);
            if (empty($department)) {
                return false;
            }
        }
        if (!empty($param['university_id'])) {
            $department->set('university_id', $param['university_id']);
        }
        if (!empty($param['name'])) {
            $department->set('name', $param['name']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $department->set('disable', $param['disable']);
        }
        if ($department->save()) {
            if (empty($department->id)) {
                $department->id = self::cached_object($department)->_original['id'];
            }
            return !empty($department->id) ? $department->id : 0;
        }
        return false;
    }

    /**
     * Add or update info for department
     *
     * @author <Cao Dinh Tuan>
     * @param array $param Input data
     * @return array Returns detail of department
     */
    public static function get_detail($param)
    {
        $query = DB::select(
            self::$_table_name . '.*', array('universities.name', 'university_name')
        )
            ->from(self::$_table_name)
            ->join('universities', 'LEFT')
            ->on('universities.id', '=', self::$_table_name . '.university_id')
            ->limit(1)
            ->offset(0);
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', '=', "{$param['id']}");
        }

        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        }
        $data = !empty($data[0]) ? $data[0] : array();
        return $data;
    }
}
