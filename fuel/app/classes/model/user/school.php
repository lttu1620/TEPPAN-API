<?php

/**
 * Any query for model User School
 *
 * @package Model
 * @created 2015-03-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_School extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'type',
        'school_delay',
        'school_studying',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'user_schools';

    /**
     * Get wareki (Japanese Calendar) - after Meiji (1912)
     *
     * @author Le Tuan Tu
     * @param $year
     * @return string
     */
    public static function get_wareki($year)
    {
        if ($year >= \Config::get('taisho_start') && $year < \Config::get('showa_start')) {
            $output = \Config::get('taisho_text') . ' ' . ($year - \Config::get('taisho_start') + 1) . '年';
        } elseif ($year >= \Config::get('showa_start') && $year < \Config::get('heisei_start')) {
            $output = \Config::get('showa_text') . ' ' . ($year - \Config::get('showa_start') + 1) . '年';
        } elseif ($year >= \Config::get('heisei_start')) {
            $output = \Config::get('heisei_text') . ' ' . ($year - \Config::get('heisei_start') + 1) . '年';
        } else {
            $output = '';
        }
        return $output;
    }

    /**
     * Add info for User Schools
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool|int|mixed
     */
    public static function add($param)
    {
        $date = $param['year'] . '-' . $param['month'] . '-' . $param['day'];
        // check date
        $dt = \DateTime::createFromFormat('Y-m-d', $date);
        if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
            static::$error_code_validation[] = array(
                'code' => \Bus\BusAbstract::ERROR_CODE_FIELD_FORMAT_DATE,
                'field' => 'date',
                'value' => $date,
            );
            return false;
        }
        // check user id and update info
        $user = Model_User::find($param['user_id']);
        if (empty($user)) {
            Model_User::errorNotExist('user_id');
            return false;
        }
        if ($user->get('birthdate') != strtotime($date)) { // only set when birthdate changed
            $user->set('birthdate', strtotime($date));
            if (!$user->update()) {
                return false;
            }
        }
        // add data to database
        for ($i = 1; $i <= 4; $i++) {
            $param['delay_'.$i] = \Config::get('years_school_delay_'.$i);
            $param['studying_'.$i] = \Config::get('years_school_studying_'.$i);
            $options['where'] = array(
                'user_id' => $param['user_id'],
                'type' => $i
            );
            $school = self::find('first', $options);
            if (empty($school)) {
                $school = new self;
                $school->set('user_id', $param['user_id']);
                $school->set('type', $i);
                $school->set('school_delay', $param['delay_'.$i]);
                $school->set('school_studying', $param['studying_'.$i]);
                if (!$school->create()) {
                    return false;
                }
            }
            $param['delay_'.$i] = $school->get('school_delay');
            $param['studying_'.$i] = $school->get('school_studying');
        }
        $data = self::get_output($param);
        return $data;
    }

    /**
     * Update info for User Schools
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns |bool
     */
    public static function update_school($param)
    {
        $date = $param['year'] . '-' . $param['month'] . '-' . $param['day'];
        // check date
        $dt = \DateTime::createFromFormat('Y-m-d', $date);
        if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
            static::$error_code_validation[] = array(
                'code' => \Bus\BusAbstract::ERROR_CODE_FIELD_FORMAT_DATE,
                'field' => 'date',
                'value' => $date,
            );
            return false;
        }
        // check user id and update info
        $user = Model_User::find($param['user_id']);
        if (empty($user)) {
            Model_User::errorNotExist('user_id');
            return false;
        }
        if ($user->get('birthdate') != strtotime($date)) { // only set when birthdate changed
            $user->set('birthdate', strtotime($date));
            if (!$user->update()) {
                return false;
            }
        }
        // set any value to param
        for ($i = 1; $i <= 4; $i++) {
            // search record to update
            $options['where'] = array(
                'user_id' => $param['user_id'],
                'type' => $i
            );
            $school = self::find('first', $options);
            if ($school) {
                if (isset($param['delay_'.$i]) || isset($param['studying_'.$i])) {
                    if (isset($param['delay_'.$i])) {
                        $school->set('school_delay', $param['delay_'.$i]);
                    }
                    if (isset($param['studying_'.$i])) {
                        $school->set('school_studying', $param['studying_'.$i]);
                    }
                    if (!$school->update()) {
                        return false;
                    }
                }
                // get data from database
                $param['delay_'.$i] = $school->get('school_delay');
                $param['studying_'.$i] = $school->get('school_studying');
            } else {
                self::errorNotExist('user_school_id');
                return false;
            }
        }
        $data = self::get_output($param);
        return $data;
    }

    /**
     * Get form output
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns
     */
    public static function get_output($param)
    {
        // base_year = input_year     (input_month >= 4)
        // base_year = input_year - 1 (input_month < 4)
        $baseYear = $param['month'] < \Config::get('first_month_of_school_year') ? $param['year'] - 1 : $param['year'];

        //output_year_1 = base_year + 7 + years_1st_school_delay
        $outputYear['1'] = $baseYear + \Config::get('before_school_year') + $param['delay_1'];

        // output_year_2 = output_year_1 + years_1st_school_studying
        $outputYear['2'] = $outputYear['1'] + $param['studying_1'];

        // output_year_3 = output_year_2 + years_2nd_school_delay
        $outputYear['3'] = $outputYear['2'] + $param['delay_2'];

        // output_year_4 = output_year_3 + years_2nd_school_studying
        $outputYear['4'] = $outputYear['3'] + $param['studying_2'];

        // output_year_5 = output_year_4 + years_3rd_school_delay
        $outputYear['5'] = $outputYear['4'] + $param['delay_3'];

        // output_year_6 = output_year_5 + years_3rd_school_studying
        $outputYear['6'] = $outputYear['5'] + $param['studying_3'];

        // output_year_7 = output_year_6 + years_4th_school_delay
        $outputYear['7'] = $outputYear['6'] + $param['delay_4'];

        // output_year_8 = output_year_7 + years_4th_school_studying
        $outputYear['8'] = $outputYear['7'] + $param['studying_4'];

        // add and get data
        $data = array();
        for ($i = 1; $i <= 8; $i++) {
            $data[] = '(' . $outputYear[$i] . ' 年) ' . self::get_wareki($outputYear[$i]) . '  ' . \Config::get('text_school_'.$i);
        }

        return $data;
    }

    /**
     * Get data to edit info
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool
     */
    public static function get_edit_info($param)
    {
        // check user id and update info
        $user = Model_User::find($param['user_id']);
        if (empty($user)) {
            Model_User::errorNotExist('user_id');
            return false;
        }
        $birthdate = $user->get('birthdate');
        if (empty($birthdate)) {
            self::errorNotExist('birthdate');
            return false;
        }
        // get data
        $data = array();
        for ($i = 1; $i <= 4; $i++) {
            // search record to update
            $options['where'] = array(
                'user_id' => $param['user_id'],
                'type' => $i
            );
            $school = self::find('first', $options);
            if ($school) {
                $data['delay_'.$i] = $school->get('school_delay');
                $data['studying_'.$i] = $school->get('school_studying');
            } else {
                self::errorNotExist('user_school_id');
                return false;
            }
        }
        return $data;
    }

    /**
     * Get detail info of User School
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns detail info of User School
     */
    public static function get_detail($param)
    {
        // check user id and update info
        $user = Model_User::find($param['user_id']);
        if (empty($user)) {
            Model_User::errorNotExist('user_id');
            return false;
        }
        $birthdate = $user->get('birthdate');
        if (empty($birthdate)) {
            self::errorNotExist('birthdate');
            return false;
        }
        // get data to param
        $param['year'] = date('Y', $user->get('birthdate'));
        $param['month'] = date('m', $user->get('birthdate'));
        $data = array();
        for ($i = 1; $i <= 4; $i++) {
            // search record to update
            $options['where'] = array(
                'user_id' => $param['user_id'],
                'type' => $i
            );
            $school = self::find('first', $options);
            if ($school) {
                $param['delay_'.$i] = $school->get('school_delay');
                $param['studying_'.$i] = $school->get('school_studying');
            } else {
                self::errorNotExist('user_school_id');
                return false;
            }
        }
        $data = self::get_output($param);
        return $data;
    }
}
