<?php

/**
 * Any query for model User Template
 *
 * @package Model
 * @created 2015-02-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Template extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'business_card_id',
        'template_id',
        'title',
        'subject',
        'content',
        'memo',
        'params',
        'language_type',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'user_templates';

    /**
     * Get list User Templates
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list User Templates
     */
    public static function get_list($param)
    {
        $param['user_id'] = !empty($param['user_id']) ? $param['user_id'] : 0;
        $query = DB::select(
            self::$_table_name . '.business_card_id',
            array(self::$_table_name . '.id', 'user_template_id'),
            array(self::$_table_name . '.template_id', 'id'),
            'templates.mail_type_id',
            'templates.mail_situation_id',
            self::$_table_name . '.title',
            self::$_table_name . '.subject',
            self::$_table_name . '.content',
            self::$_table_name . '.memo',
            self::$_table_name . '.params',
            self::$_table_name . '.language_type',
            array('templates.memo', 'memo_template'),
            'templates.is_public',
            'templates.made_by',
            'templates.favorite_count',
            'templates.disable',
            self::$_table_name . '.created',
            self::$_table_name . '.updated',
            DB::expr("
                    (CASE user_templates.language_type
                        WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_types.name
                        WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_types.name_en
                        WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_types.name_cn
                    END) as mail_type_name
                "),
            DB::expr("
                    (CASE user_templates.language_type
                        WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_situations.name
                        WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_situations.name_en
                        WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_situations.name_cn
                    END) as mail_situation_name
                "),
            array('users.name', 'username_made_by'),
            DB::expr("
                    IF (ISNULL(favorites.id), 0, 1) AS is_favorite
                ")
        )
            ->from(self::$_table_name)
            ->join('templates', 'LEFT')
            ->on(self::$_table_name . '.template_id', '=', 'templates.id')
            ->join('mail_types', 'LEFT')
            ->on('templates.mail_type_id', '=', 'mail_types.id')
            ->join('mail_situations', 'LEFT')
            ->on('templates.mail_situation_id', '=', 'mail_situations.id')
            ->join('users', 'LEFT')
            ->on('templates.made_by', '=', 'users.id')
            ->join(DB::expr("(
				SELECT *
				FROM   template_favorites
				WHERE  disable = 0
					   AND user_id = {$param['user_id']}
			) favorites"), 'LEFT')
            ->on('templates.id', '=', 'favorites.template_id')
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (!empty($param['template_id'])) {
            $query->where(self::$_table_name . '.template_id', '=', $param['template_id']);
        }
        $query->order_by(self::$_table_name . '.id', 'DESC');
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get detail User Template
     *
     * @author Tuancd
     * @param array $param Input data
     * @return array Returns array detail User Template
     */
    public static function get_detail($param)
    {
        $result = self::find($param['id']);
        if (empty($result)) {
            static::errorNotExist('user_template', $param['id']);
        }
        return $result;
    }

    /**
     * Update info for User Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns user template id or false if error
     */
    public static function set_update($param)
    {
        $template = self::find($param['id']);
        if (empty($template)) {
            static::errorNotExist('user_template_id', $param['id']);
            return false;
        }

        if (!empty($param['business_card_id'])) {
            $template->set('business_card_id', $param['business_card_id']);
        }
        if (!empty($param['title'])) {
            $template->set('title', $param['title']);
        }
        if (!empty($param['subject'])) {
            $template->set('subject', $param['subject']);
        }
        if (!empty($param['content'])) {
            $template->set('content', $param['content']);
        }
        if (!empty($param['memo'])) {
            $template->set('memo', $param['memo']);
        }
        if (!empty($param['params'])) {
            $template->set('params', $param['params']);
        }
        if (!empty($param['language_type'])) {
            $template->set('language_type', $param['language_type']);
        }
        if ($template->update()) {
            return $template->id;
        }
        return false;
    }

    /**
     * Disable/enable User Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) { // disable in admin
            $ids = explode(',', $param['id']);
            foreach ($ids as $id) {
                $options['where'][] = array(
                    'id' => $id
                );
                if (!empty($param['user_id'])) {
                    $options['where'][] = array(
                        'user_id' => $param['user_id']
                    );
                }
                $template = self::find('first', $options);
                if ($template) {
                    $template->set('disable', $param['disable']);
                    if (!$template->update()) {
                        return false;
                    }
                } else {
                    static::errorNotExist('user_template_id', $id);
                    return false;
                }
            }
        } else { // disable in mobile
            $options['where'][] = array(
                'template_id' => $param['template_id'],
                'user_id'     => $param['user_id'],
                'disable'     => '0'
            );

            $template = self::find('first', $options);
            if ($template) {
                $template->set('disable', $param['disable']);
                if (!$template->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('user_template_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get all User Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns array all User Template
     */
    public static function get_all($param)
    {
        $param['user_id'] = !empty($param['user_id']) ? $param['user_id'] : 0;
        $query = DB::select(
            self::$_table_name . '.business_card_id',
            array(self::$_table_name . '.id', 'id'),
            'templates.mail_type_id',
            'templates.mail_situation_id',
            self::$_table_name . '.title',
            self::$_table_name . '.subject',
            self::$_table_name . '.content',
            self::$_table_name . '.memo',
            self::$_table_name . '.params',
            self::$_table_name . '.language_type',
            array('templates.memo', 'memo_template'),
            'templates.is_public',
            'templates.made_by',
            'templates.favorite_count',
            'templates.disable',
            'templates.created',
            'templates.updated',
            DB::expr("
				(CASE user_templates.language_type
					WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_types.name
					WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_types.name_en
					WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_types.name_cn
				END) as mail_type_name
			"),
            DB::expr("
				(CASE user_templates.language_type
					WHEN '" . \Config::get('language_type')['jp'] . "' THEN mail_situations.name
					WHEN '" . \Config::get('language_type')['en'] . "' THEN mail_situations.name_en
					WHEN '" . \Config::get('language_type')['cn'] . "' THEN mail_situations.name_cn
				END) as mail_situation_name
			"),
            array('users.name', 'username_made_by'),
            DB::expr("
				IF (ISNULL(favorites.id), 0, 1) AS is_favorite
			")
        )
            ->from(self::$_table_name)
            ->join('templates', 'LEFT')
            ->on(self::$_table_name . '.template_id', '=', 'templates.id')
            ->join('mail_types', 'LEFT')
            ->on('templates.mail_type_id', '=', 'mail_types.id')
            ->join('mail_situations', 'LEFT')
            ->on('templates.mail_situation_id', '=', 'mail_situations.id')
            ->join('users', 'LEFT')
            ->on('templates.made_by', '=', 'users.id')
            ->join(DB::expr("(
				SELECT *
				FROM   template_favorites
				WHERE  disable = 0
					   AND user_id = {$param['user_id']}
			) favorites"), 'LEFT')
            ->on('templates.id', '=', 'favorites.template_id')
            ->where(self::$_table_name . '.disable', '=', '0');

        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (!empty($param['template_id'])) {
            $query->where(self::$_table_name . '.template_id', '=', $param['template_id']);
        }
        $query->order_by(self::$_table_name . '.id', 'DESC');
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Add info for user template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns user template id or false if error
     */
    public static function add($param)
    {
        if (!empty($param['template_id'])) {
            $query = DB::select(
                array('templates.id', 'template_id')
            )
                ->from('templates')
                ->where('templates.id', '=', $param['template_id'])
                ->where('templates.disable', '=', '0');
            $data = $query->execute()->offsetGet(0);
            if (empty($data['template_id'])) {
                static::errorNotExist('template_id', $param['template_id']);
                return false;
            }
        }
        $template = new self;
        $template->set('user_id', $param['user_id']);
        $template->set('title', $param['title']);
        $template->set('content', $param['content']);
        $template->set('language_type', $param['language_type']);
        if (!empty($param['business_card_id'])) {
            $template->set('business_card_id', $param['business_card_id']);
        }
        if (!empty($param['subject'])) {
            $template->set('subject', $param['subject']);
        }
        if (!empty($param['params'])) {
            $template->set('params', $param['params']);
        }
        if (!empty($param['template_id'])) {
            $template->set('template_id', $param['template_id']);
        }
        if (!empty($param['memo'])) {
            $template->set('memo', $param['memo']);
        }
        if ($template->create()) {
            $template->id = self::cached_object($template)->_original['id'];
            return $template->id;
        }
        return false;
    }
    
    /**
     * Get for used templates report.
     *
     * @author thailh
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_for_report($param)
    {
        $query = DB::select(
                'templates.id',
                'templates.title',
                DB::expr("
                    COUNT(" . self::$_table_name . ".user_id) AS count_used
                ")
            )
            ->from(self::$_table_name)
            ->join('templates')
            ->on(self::$_table_name . '.template_id', '=', 'templates.id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('templates.disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.updated', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.updated', '<=', self::date_to_val($param['date_to']));
        }
        $query->group_by(self::$_table_name . '.template_id');
        $query->order_by('count_used', 'DESC');
        $query->limit(10)->offset(0);
        $data = $query->execute()->as_array();
        return $data;
    }

}
