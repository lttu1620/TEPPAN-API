<?php

use Fuel\Core\DB;

/**
 * <Model_User_Setting - Model to operate to User_Setting's functions>
 *
 * @package Model
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_User_Setting extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'setting_id',
        'value',
        'disable',
        'created',
        'updated',
    );
    protected static $not_checks = array('id', 'created', 'updated');
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'user_settings';
    protected static $_setting_type = 'user';

    /**
     * Add_update - function to add or update a user_settings
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Returns setting id or false if error
     */
    public static function add_update($param) {
        //check if user_id exist or not
        $options['where'] = array(
            'id' => $param['user_id'],
        );
        $data = Model_User::find('all', $options);
        if (empty($data)) {//if user_id not exist
            static::errorNotExist('user_id', $param['user_id']);
            return false;
        } else {//if user_id exist
            $values = json_decode($param['value']);
            foreach ($values as $val) {
                $options['where'] = array(
                    'user_id' => $param['user_id'],
                    'setting_id' => $val->setting_id
                );
                //check if update or insert
                $user_setting = self::find('first', $options);
                if (!empty($user_setting)) {//if exist then update
                    $user_setting->set('user_id', $param['user_id']);
                    $user_setting->set('setting_id', $val->setting_id);
                    $user_setting->set('value', $val->value);
                    $user_setting->update();
                } else {//if not exist then insert
                    $us = new self;
                    $us->set('user_id', $param['user_id']);
                    $us->set('setting_id', $val->setting_id);
                    $us->set('value', $val->value);
                    $us->save();
                }
            }
        }
        return true;
    }

    /**
     * Disable - function to disable or enable a user_setting
     *
     * @author diennvt
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $userSetting = self::find($id);
            if (empty($userSetting)) {
                return false;
            }
            $userSetting->set('disable', $param['disable']);
            if (!$userSetting->update()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get all - function to get all user_setting
     *
     * @author <diennvt> 
     * @param array $param Input data
     * @return array Returns array all user setting
     */
    public static function get_all($param) {
        $options['where'] = array(
            'id' => $param['user_id'],
        );
        $res = Model_User::find('all', $options);
        if (empty($res)) {
            static::errorNotExist('user_id', $param['user_id']);
            return false;
        } else {
            $query = DB::select(
                            array('settings.id', 'setting_id'),
                            'settings.name', 
                            'settings.description', 
                            'settings.data_type', 
                            DB::expr("IF(ISNULL(" . self::$_table_name . '.value' . "), settings.value, " . self::$_table_name . '.value' . ") as value"), 
                            DB::expr("IF(ISNULL(" . self::$_table_name . '.id' . "), settings.id, " . self::$_table_name . '.id' . ") as id"), 
                            self::$_table_name . '.disable'
                    )
                    ->from('settings')
                    ->join(DB::expr("(SELECT * FROM " . self::$_table_name . " WHERE user_id = {$param['user_id']}) AS " . self::$_table_name), 'LEFT')
                    ->on('settings.id', '=', self::$_table_name . '.setting_id')
                    ->where('settings.type', self::$_setting_type)
                     ->order_by('settings.name');

            if (!empty($param['name'])) {
                $query->where('settings.name', 'LIKE', "{$param['name']}%");
            }
            if (!empty($param['data_type'])) {
                $query->where('settings.data_type', $param['data_type']);
            }
            if (isset($param['disable']) && $param['disable'] != '') {
                $query->where('settings.disable', $param['disable']);
            }
            if (!empty($param['sort'])) {
                $sortExplode = explode('-', $param['sort']);
                if ($sortExplode[0] == 'created') {
                    $sortExplode[0] = self::$_table_name . '.created';
                }
                $query->order_by($sortExplode[0], $sortExplode[1]);
            } else {
                $query->order_by(self::$_table_name . '.created', 'DESC');
            }
            if (!empty($param['page']) && !empty($param['limit'])) {
                $offset = ($param['page'] - 1) * $param['limit'];
                $query->limit($param['limit'])->offset($offset);
            }
            $data = $query->execute()->as_array();
            $total = !empty($data) ? DB::count_last_query() : 0;
            return array($total, $data);
        }
    }

    /**
     * Multi_update - function to add or update a user settings
     *
     * @author <diennvt> 
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function multi_update($param) {
        $upd_data = json_decode($param['value']);

        if (empty($upd_data)) {
            AppLog::info("Empty parameter", __METHOD__, $upd_data);
            return false;
        }
        //set information
        foreach ($upd_data as $row) {
              $options['where'] = array(
                'user_id' => $row->user_id,
                'setting_id' => $row->setting_id
            );
            $userSetting = self::find('first', $options);
            if (empty($userSetting)) {
                $us = new self;
                $us->set('user_id', $row->user_id);
                $us->set('setting_id', $row->setting_id);
                $us->set('value', $row->value);
                if (!$us->save()) {
                    AppLog::info("User setting insert failed", __METHOD__, $row);
                    return false;
                }
            } else {
                foreach ($row as $key => $val) {
                    if (in_array($key, self::$_properties) && !in_array($key, self::$not_checks)) {
                        if ($val != '') {
                            $userSetting->set($key, $val);
                        }
                    }
                }
                if (!$userSetting->update()) {
                    AppLog::info("User setting update failed", __METHOD__, $row);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Mobile_get_list - function to get list user_setting
     *
     * @author <tuancd> 
     * @param array $param Input data
     * @return array Returns list user setting
     */
    public static function mobile_get_list($param) {
        $options['where'] = array(
            'id' => $param['user_id'],
        );
        $res = Model_User::find('all', $options);
        if (empty($res)) {
            static::errorNotExist('user_id', $param['user_id']);
            return false;
        } else {
            // Get all user_setting for user
            $query = DB::select('settings.name', DB::expr("IF(ISNULL(" . self::$_table_name . '.value' . "), settings.value, " . self::$_table_name . '.value' . ") as value"))
                    ->from('settings')
                    ->join(DB::expr("(SELECT * FROM " . self::$_table_name . " WHERE user_id = {$param['user_id']}) AS " . self::$_table_name), 'LEFT')
                    ->on('settings.id', '=', self::$_table_name . '.setting_id')
                    ->where('settings.disable', 0)
                    ->where('settings.type', self::$_setting_type)
                    ->where('settings.name', 'in', Config::get('mobile_user_setting_key', true));
            $data = $query->execute()->as_array();
            return \Lib\Arr::key_value($data, 'name', 'value');
        }
    }

    /**
     * <mobile_update - function to update user_setting for mobile>   
     *
     * @author <tuancd> 
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function mobile_update($param) {
        $user_id = $param['user_id'];
        unset($param['user_id']);
        $options['where'] = array(
            'id' => $user_id,
        );
        $res = Model_User::find('all', $options);
        if (empty($res)) {
            static::errorNotExist('user_id', $param['user_id']);
            return false;
        } else {
            foreach ($param as $key => $val) {
                if (isset($val) && $key != 'os') {
                    try {
                        //check user_setting update or insert by name of settings
                        $query = DB::select(array('settings.id', 'setting_id'), array(self::$_table_name . '.id', 'user_setting_id'))
                                ->from('settings')
                                ->join(self::$_table_name)
                                ->on('settings.id', '=', self::$_table_name . '.setting_id')
                                ->where('settings.type', self::$_setting_type)
                                ->where('settings.name', $key);

                        if ($data = $query->execute()->as_array()) {
                            $user_setting = self::find($data[0]['user_setting_id']);
                            if (!empty($user_setting)) {//if exist then update
                                $user_setting->set('user_id', $user_id);
                                $user_setting->set('setting_id', $user_setting['setting_id']);
                                $user_setting->set('value', $val);
                                $user_setting->update();
                            }
                        } else {//if not exist then insert by search setting_id by name
                            $condition['where'] = array('name' => $key);
                            $setting = \Model_Setting::find('first', $condition);
                            if (!empty($setting)) {
                                $us = new self;
                                $us->set('user_id', $user_id);
                                $us->set('setting_id', $setting['id']);
                                $us->set('value', $val);
                                $us->save();
                            }
                        }
                    } catch (Exception $e) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

}
