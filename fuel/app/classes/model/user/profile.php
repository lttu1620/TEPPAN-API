<?php

use Fuel\Core\DB;
use Lib\Util;

/**
 * Any query in Model User Profile
 *
 * @package Model
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Profile extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'name',
        'language_type',
        'first_name',
        'last_name',
        'university_name',
        'department_name',
        'major',
        'course',
        'city',
        'address',
        'disable',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'user_profiles';

    /**
     * Get list user profile
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list user profile
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        if (!empty($param['id'])) {
            $query->where('id', '=', $param['id']);
        }
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        if (!empty($param['email'])) {
            $query->where('email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Add or update info for user profile
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns user profile id or false if error
     */
    public static function add_update($param)
    {
        if (!empty($param['id'])) {
            $profile = self::find($param['id']);
            if (empty($profile)) {
                static::errorNotExist('profile_id');
                return false;
            }
        } else {
            if (!empty($param['user_id'])) {
                $options['where'][] = array(
                    'user_id'       => $param['user_id'],
                    'language_type' => $param['language_type']
                );
                $profile = self::find('first', $options);
                if (empty($profile)) {
                    $profile = new self;
                }
            }
        }
        if (!empty($param['user_id'])) {
            $profile->set('user_id', $param['user_id']);
        }
        $profile->set('name', $param['name']);
        if (!empty($param['language_type'])) {
            $profile->set('language_type', $param['language_type']);
        }
        if (!empty($param['first_name'])) {
            $profile->set('first_name', $param['first_name']);
        }
        if (!empty($param['last_name'])) {
            $profile->set('last_name', $param['last_name']);
        }
        if (!empty($param['university_name'])) {
            $profile->set('university_name', $param['university_name']);
        }
        if (!empty($param['department_name'])) {
            $profile->set('department_name', $param['department_name']);
        }
        if (!empty($param['major'])) {
            $profile->set('major', $param['major']);
        }
        if (!empty($param['course'])) {
            $profile->set('course', $param['course']);
        }
        if (!empty($param['city'])) {
            $profile->set('city', $param['city']);
        }
        if (!empty($param['address'])) {
            $profile->set('address', $param['address']);
        }
        if ($profile->save()) {
            if (empty($profile->id)) {
                $profile->id = self::cached_object($profile)->_original['id'];
            }
            return !empty($profile->id) ? $profile->id : 0;
        }
        return false;
    }

    /**
     * Update password by token
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function update_password($param)
    {
        $query = DB::select(
            array('user_activations.email', 'activation_email'),
            array('user_activations.token', 'token'),
            array('user_activations.regist_type', 'regist_type'),
            array('user_activations.disable', 'activation_disable'),
            array('user_activations.id', 'activation_id'),
            self::$_table_name . '.*'
        )
            ->from('user_activations')
            ->join(self::$_table_name, 'LEFT')
            ->on('user_activations.email', '=', self::$_table_name . '.email')
            ->where('user_activations.token', '=', $param['token'])
            ->where('user_activations.regist_type', '=', $param['regist_type'])
            ->where('user_activations.disable', '=', '0');
        $data = $query->execute()->as_array();
        if ($data) {
            if (isset($data[0]['id']) && $data[0]['id']) {
                $user = self::find($data[0]['id']);
                if ($user) {
                    $user->set('password', Util::encodePassword($param['password'], $user->get('email')));
                    if ($user->update()) {
                        if (!\Model_User_Activation::disable(array(
                                                                 'id'      => $data[0]['activation_id'],
                                                                 'disable' => '1'
                                                             ))
                        ) {
                            return false;
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Change password by user_id
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function change_password($param)
    {

        $query = DB::select()
            ->from(self::$_table_name)
            ->where('user_id', '=', $param['user_id']);
        $data = $query->execute()->as_array();
        if ($data) {
            if (isset($data[0]['id']) && $data[0]['id']) {
                $user = self::find($data[0]['id']);
                if ($user) {
                    $user->set('password', Util::encodePassword($param['password'], $user['email']));
                    if ($user->update()) {
                        return true;
                    }
                }
            }
        }
        self::errorNotExist('user_id');
        return false;

    }
}
