<?php

/**
 * Any query for model Business Card
 *
 * @package Model
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Business_Card extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'image',
        'company',
        'department',
        'name',
        'phone',
        'email',
        'postal_code',
        'address',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'business_cards';

    /**
     * Get list Business Card
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list Business Card
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'username')
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id');

	
	if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (!empty($param['username'])) {
            $query->where('users.name', 'LIKE', "%{$param['username']}%");
        }
        if (!empty($param['company'])) {
            $query->where(self::$_table_name . '.company', 'LIKE', "%{$param['company']}%");
        }
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['phone'])) {
            $query->where(self::$_table_name . '.phone', '=', $param['phone']);
        }
        if (!empty($param['email'])) {
            $query->where(self::$_table_name . '.email', 'LIKE', "%{$param['email']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data  = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Get detail of Business Card
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns detail of Business Card
     */
    public static function get_detail($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'username')
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->where(self::$_table_name . '.id', '=', $param['id']);
        $data  = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        }

        return $data ? $data[0] : array();
    }

    /**
     * Add and update info for Business Card
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns card id or false if error
     */
    public static function add_update($param)
    {
        $id   = !empty($param['id']) ? $param['id'] : 0;
        $card = new self;
        if (!empty($id)) {
            $card = self::find($id);
            if (empty($card)) {
                static::errorNotExist('card_id', $id);
                return false;
            }
        }

        if (!empty($param['user_id'])) {
            $card->set('user_id', $param['user_id']);
        }
        if (!empty($param['image'])) {
            $card->set('image', $param['image']);
        }
        if (!empty($param['company'])) {
            $card->set('company', $param['company']);
        }
        if (!empty($param['department'])) {
            $card->set('department', $param['department']);
        }
        if (!empty($param['name'])) {
            $card->set('name', $param['name']);
        }
        if (!empty($param['phone'])) {
            $card->set('phone', $param['phone']);
        }
        if (!empty($param['email'])) {
            $card->set('email', $param['email']);
        }
        if (isset($param['post_code']) && $param['post_code']!= '') {
            $card->set('post_code', $param['post_code']);
        }
        if (!empty($param['address'])) {
            $card->set('address', $param['address']);
        }

        if ($card->save()) {
            if (empty($card->id)) {
                $card->id = self::cached_object($card)->_original['id'];
            }
            return !empty($card->id) ? $card->id : 0;
        }
        return false;
    }

    /**
     * Disable/enable Business Card
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $card = self::find($id);
            if ($card) {
                $card->set('disable', $param['disable']);
                if (!$card->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('card_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Get all Business Card
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns array all Business Card
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'username')
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id');
        $query->order_by(self::$_table_name . '.id', 'ASC');
        $data = $query->execute()->as_array();
        return $data;
    }
}
