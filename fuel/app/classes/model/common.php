<?php

class Model_Common extends Model_Abstract {

    public static function disableBook($param) {
        $status = !empty($param['status']) ? $param['status'] : 0;
        $id = !empty($param['id']) ? $param['id'] : 0;
        $book = Model_Book::find($id);
        $book->is_active = $status;
        if (!$book->save()) {
            return false;
        }
        return true;
    }
    
    public static function trigger() {        
        $query = DB::query("select * from information_schema.triggers where trigger_schema = 'cakephp'");
        $triggers = $query->execute()->as_array();
        $sql = '';
        foreach ($triggers as $trigger) {
            if (in_array(
                $trigger['EVENT_OBJECT_TABLE'], 
                array('books', 'authors', 'mail_situations', 'news_feed_import_logs_2015_01-27', 'news_feeds_backup_2014_01_23'))) {
                continue;
            }
            if ($trigger['ACTION_TIMING'] == 'BEFORE') {
            $sql .= "
                DROP TRIGGER IF EXISTS `{$trigger['TRIGGER_NAME']}`;
                CREATE TRIGGER `{$trigger['TRIGGER_NAME']}` {$trigger['ACTION_TIMING']} {$trigger['EVENT_MANIPULATION']} ON `{$trigger['EVENT_OBJECT_TABLE']}` FOR EACH ROW 
                {$trigger['ACTION_STATEMENT']};
            ";  }           
        }
        return $sql;
    }

}
