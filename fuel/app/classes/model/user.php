<?php

use Lib\Str;

/**
 * Model_User - Model to operate to User's functions>
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_User extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'app_id',
        'name',
        'first_name',
        'last_name',
        'first_name_kana',
        'last_name_kana',
        'birthdate',
        'image',
        'university_id',
        'campus_id',
        'department_id',
        'university_name',
        'department_name',
        'major',
        'course',
        'grade_id',
        'sex_id',
        'grade',
        'postcode',
        'prefecture_id',
        'city',
        'address',
        'tel',
        'tel_mobile',
        'mail',
        'mail_mobile',
        'signature',
        'high_graduate_year',
        'univ_graduate_year',
        'device_id',
        'is_ios',
        'is_android',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'users';

    /**
     * Get list - function to get a list of user
     *
     * @author diennvt
     * @param array $param Input data
     * @return array Returns list of user
     */
    public static function get_list($param)
    {
        $query = DB::select(
            'users.id',
            DB::expr("
                IFNULL(IF(users.image='',NULL,users.image),'" . \Config::get('no_image_other') . "') AS image
            "),
            'user_profiles.name',
            'users.sex_id',
            'users.mail',
            'is_ios',
            'is_android',
            DB::expr("
                IF(
                    IF(
                        users.university_id = '', NULL, users.university_id
                    ), universities.name, user_profiles.university_name
                ) AS university_name
            "),
            array('campuses.name', 'campus_name'),
            DB::expr("
                IF(
                    IF(
                        users.department_id = '', NULL, users.department_id
                    ), departments.name, user_profiles.department_name
                ) AS department_name
            "),
            'users.created',
            'users.disable'
        )
            ->from(self::$_table_name)
            ->join('universities', 'LEFT')
            ->on(self::$_table_name . '.university_id', '=', 'universities.id')
            ->join('campuses', 'LEFT')
            ->on(self::$_table_name . '.campus_id', '=', 'campuses.id')
            ->join('departments', 'LEFT')
            ->on(self::$_table_name . '.department_id', '=', 'departments.id')
            ->join(DB::expr("
                (SELECT user_profiles.*
                FROM  user_profiles
                WHERE language_type = {$param['language_type']}
                AND   disable = 0) AS user_profiles
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'user_profiles.user_id');
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where('user_profiles.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['university_id'])) {
            $query->where(self::$_table_name . '.university_id', '=', $param['university_id']);
        }
        if (!empty($param['campus_id'])) {
            $query->where('campus_id', '=', $param['campus_id']);
        }
        if (!empty($param['department_id'])) {
            $query->where('department_id', '=', $param['department_id']);
        }
        if (!empty($param['department_name'])) {
            $query->where('user_profiles.department_name', 'LIKE', "%{$param['department_name']}%");
        }
        if (!empty($param['mail'])) {
            $query->where('mail', 'LIKE', "%{$param['mail']}%");
        }
        if (!empty($param['sex_id'])) {
            $query->where('sex_id', $param['sex_id']);
        }
        if (!empty($param['tel'])) {
            $query->where(self::$_table_name . '.tel', 'LIKE', "%{$param['tel']}%");
        }
        if (!empty($param['high_graduate_year'])) {
            $query->where('high_graduate_year', $param['high_graduate_year']);
        }
        if (!empty($param['univ_graduate_year'])) {
            $query->where('univ_graduate_year', $param['univ_graduate_year']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Disable - function to disable or enable a user
     *
     * @author <diennvt>
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $user = self::find($id);
            if (empty($user)) {
                return false;
            }
            $user->set('disable', $param['disable']);
            if (!$user->update()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add update - function to add or update a user
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Returns user id or false if error
     */
    public static function add_update($param)
    {
        //check id if existing
        $id = !empty($param['id']) ? $param['id'] : 0;
        $user = new self;
        if (!empty($id) || !empty($param['user_id'])) {
            if (!empty($id)) {
                $options['where'] = array(
                    'id' => $id
                );
            }
            if (!empty($param['user_id'])) {
                $options['where'] = array(
                    'id' => $param['user_id']
                );
            }
            $user = self::find('first', $options);
            if (empty($user)) {
                static::errorNotExist('user_id');
                return false;
            }
        } elseif (!empty($param['os'])) { // add new
            if ($param['os'] == \Config::get('os')['ios']) {
                $user->set('is_ios', '1');
            } elseif ($param['os'] == \Config::get('os')['android']) {
                $user->set('is_android', '1');
            } else {
                $user->set('is_android', '0');
                $user->set('is_ios', '0');
            }
        }
        // set information for user if having
        if (empty($param['name'])) {
            $param['name'] = '';
        }
        if (!empty($param['name'])) {
            $user->set('name', $param['name']);
        }
        if (!empty($param['app_id'])) {
            $user->set('app_id', $param['app_id']);
        }
        if (!empty($param['image'])) {
            $user->set('image', $param['image']);
        }
        if (!empty($param['first_name_kana'])) {
            $user->set('first_name_kana', $param['first_name_kana']);
        }
        if (!empty($param['last_name_kana'])) {
            $user->set('last_name_kana', $param['last_name_kana']);
        }
        if (!empty($param['university_id'])) {
            $user->set('university_id', $param['university_id']);
        }
        if (!empty($param['campus_id'])) {
            $user->set('campus_id', $param['campus_id']);
        }
        if (!empty($param['department_id'])) {
            $user->set('department_id', $param['department_id']);
        }
        if (!empty($param['grade_id'])) {
            $user->set('grade_id', $param['grade_id']);
        }
        if (!empty($param['sex_id'])) {
            $user->set('sex_id', $param['sex_id']);
        }
        if (!empty($param['grade'])) {
            $user->set('grade', $param['grade']);
        }
        if (!empty($param['postcode'])) {
            $user->set('postcode', $param['postcode']);
        }
        if (!empty($param['prefecture_id'])) {
            $user->set('prefecture_id', $param['prefecture_id']);
        }
        if (!empty($param['tel'])) {
            $user->set('tel', $param['tel']);
        }
        if (!empty($param['tel_mobile'])) {
            $user->set('tel_mobile', $param['tel_mobile']);
        }
        if (!empty($param['mail'])) {
            $user->set('mail', $param['mail']);
        }
        if (!empty($param['mail_mobile'])) {
            $user->set('mail_mobile', $param['mail_mobile']);
        }
        if (!empty($param['signature'])) {
            $user->set('signature', $param['signature']);
        }
        if (!empty($param['high_graduate_year'])) {
            $user->set('high_graduate_year', $param['high_graduate_year']);
        }
        if (!empty($param['univ_graduate_year'])) {
            $user->set('univ_graduate_year', $param['univ_graduate_year']);
        }
        if (!empty($param['device_id'])) {
            $user->set('device_id', $param['device_id']);
        }
        //check id for adding new or updating
        if ($user->save()) {
            if (empty($id)) {
                $user->id = self::cached_object($user)->_original['id'];
                $user->app_id = \Lib\str::generate_app_id($user->id);
                if ($user->update()) {
                    $param['user_id'] = $user->id;
                    if (!\Model_User_Profile::add_update($param)) {
                        return false;
                    }
                    return $user->id;
                }
            }
            $param['user_id'] = $user->id;
            unset($param['id']);
            if (!\Model_User_Profile::add_update($param)) {
                return false;
            }
            return !empty($user->id) ? $user->id : 0;
        }
        return false;
    }

    /**
     * Get detail - function to get detail a user
     *
     * @author diennvt
     * @param array $param Input data
     * @return array Returns detail of user
     */
    public static function get_detail($param)
    {
        $query = DB::select(
            self::$_table_name . '.id',
            self::$_table_name . '.app_id',
            DB::expr("
                {$param['language_type']} AS language_type
            "),
            'user_profiles.name',
            'user_profiles.first_name',
            'user_profiles.last_name',
            self::$_table_name . '.first_name_kana',
            self::$_table_name . '.last_name_kana',
            DB::expr("
                IF(
                    IF(
                        users.university_id = '', NULL, users.university_id
                    ), universities.name, user_profiles.university_name
                ) AS university_name
            "),
            array('campuses.name', 'campus_name'),
            DB::expr("
                IF(
                    IF(
                        users.department_id = '', NULL, users.department_id
                    ), departments.name, user_profiles.department_name
                ) AS department_name
            "),
            DB::expr("
                IFNULL(IF(users.image='',NULL,users.image),'" . \Config::get('no_image_other') . "') AS image
            "),
            'user_profiles.major',
            'user_profiles.course',
            'user_profiles.city',
            'user_profiles.address',
            self::$_table_name . '.university_id',
            self::$_table_name . '.campus_id',
            self::$_table_name . '.department_id',
            self::$_table_name . '.grade_id',
            self::$_table_name . '.sex_id',
            self::$_table_name . '.grade',
            self::$_table_name . '.postcode',
            self::$_table_name . '.prefecture_id',
            self::$_table_name . '.tel',
            self::$_table_name . '.tel_mobile',
            self::$_table_name . '.mail',
            self::$_table_name . '.mail_mobile',
            self::$_table_name . '.signature',
            self::$_table_name . '.high_graduate_year',
            self::$_table_name . '.univ_graduate_year',
            self::$_table_name . '.device_id',
            self::$_table_name . '.created',
            self::$_table_name . '.disable'
        )
            ->from(self::$_table_name)
            ->join('universities', 'LEFT')
            ->on(self::$_table_name . '.university_id', '=', 'universities.id')
            ->join('campuses', 'LEFT')
            ->on(self::$_table_name . '.campus_id', '=', 'campuses.id')
            ->join('departments', 'LEFT')
            ->on(self::$_table_name . '.department_id', '=', 'departments.id')
            ->join(DB::expr("
                (SELECT user_profiles.*
                FROM  user_profiles
                WHERE language_type = {$param['language_type']}
                AND   disable = 0) AS user_profiles
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'user_profiles.user_id')
            ->join('prefectures', 'LEFT')
            ->on(self::$_table_name . '.prefecture_id', '=', 'prefectures.id')
            ->where(self::$_table_name . '.id', '=', "{$param['id']}");
        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('user_id');
        }
        return !empty($data[0]) ? $data[0] : array();
    }

    /**
     * Register user
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function register($param)
    {
        $user = new self;
        $user->set('name', $param['name']);
        $user->set('grade', $param['grade']);
        if ($user->create()) {
            $user->id = self::cached_object($user)->_original['id'];
            $user->app_id = \Lib\str::generate_app_id($user->id);
            if ($user->update()) {
                return array(
                    'id'     => $user->id,
                    'app_id' => $user->app_id
                );
            }
        }
        return false;
    }

    /**
     * Get number user
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int number user
     */
    public static function get_number($param)
    {
        $query = DB::select(
            DB::expr("
                COUNT(id) as number
            ")
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['from_date'])) {
            $query->where(self::$_table_name . '.created', '>=', $param['from_date']);
        }
        if (!empty($param['to_date'])) {
            $query->where(self::$_table_name . '.created', '<=', $param['to_date']);
        }
        $data = $query->execute()->offsetGet(0);
        return $data['number'];
    }
    
    /**
     * Get general report from user, favorites, likes.
     *
     * @author thailh
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_general_report($param)
    {        
        
        $data = self::get_user_report($param);
        $data['used_templates'] = Model_Template_Open_Log::get_for_report($param);
        $param['date_from'] = date('Y-m-d', strtotime('first day of this month')); //$param['date_from'] = date('Y-1-1');
        $param['date_to'] = date('Y-m-d');        
        $data['favorited_templates'] = Model_Template_Favorite::get_for_report($param); 
        $data['monthly_used_templates'] = Model_Template_Open_Log::get_for_report($param);
        return $data;
    }

    /**
     * Get user report.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return aray Returns the array.
     */
    public static function get_user_report($param)
    {
        $query = DB::select(
                DB::expr("            
                    COUNT(*) AS user_count,
                    IFNULL(SUM(IF(IFNULL(mail, '')<>'', 1, 0)), 0) AS user_registed_count,
                    IFNULL(SUM((CASE WHEN users.is_android = 1 THEN 1 ELSE 0 END)), 0) AS user_android_count,
                    IFNULL(SUM((CASE WHEN users.is_ios = 1 THEN 1 ELSE 0 END)), 0) AS user_ios_count
                ")
            )
            ->from(self::$_table_name)                      
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $result = $query->execute()->offsetGet(0);        
        return $result;             
    }
    
}
