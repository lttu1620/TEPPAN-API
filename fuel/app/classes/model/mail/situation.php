<?php

/**
 * Any query for model Mail Situation
 *
 * @package Model
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Mail_Situation extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'name',
		'name_en',
		'name_cn',
		'template_count',
		'image_url',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'          => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'          => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'mail_situations';

	/**
	 * Get list Mail Situations
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return array Returns list Mail Situations
	 */
	public static function get_list($param)
	{
		$sql = "
			SELECT id,
				   name,
				   name_en,
				   name_cn,
				   image_url,
				   disable,
				   created,
				   updated,
				   SUM(jp + en + cn) AS template_count,
				   SUM(jp) AS template_count_jp,
				   SUM(en) AS template_count_en,
				   SUM(cn) AS template_count_cn
			FROM   (SELECT mail_situations.*,
						   IF (language_type = '" . \Config::get('language_type')['jp'] . "', 1, 0) AS jp,
						   IF (language_type = '" . \Config::get('language_type')['en'] . "', 1, 0) AS en,
						   IF (language_type = '" . \Config::get('language_type')['cn'] . "', 1, 0) AS cn
					FROM   mail_situations
						   LEFT JOIN templates
							 ON mail_situations.id = templates.mail_situation_id) templates
							 WHERE 1 = 1
		";
		if (!empty($param['name'])) {
			$sql .= " AND name LIKE '%{$param['name']}%' ";
		}
		if (!empty($param['name_en'])) {
			$sql .= " AND name LIKE '%{$param['name_en']}%' ";
		}
		if (!empty($param['name_cn'])) {
			$sql .= " AND name LIKE '%{$param['name_cn']}%' ";
		}
		if (isset($param['disable']) && $param['disable'] != '') {
			$sql .= " AND disable = {$param['disable']} ";
		}
		$sql .= "
			GROUP  BY id,
					  name,
					  name_en,
					  name_cn
		";
		if (!empty($param['sort'])) {
			$sortExplode = explode('-', $param['sort']);
			$sql .= " ORDER BY {$sortExplode[0]} {$sortExplode[1]} ";
		} else {
			$sql .= " ORDER BY created DESC ";
		}
		if (!empty($param['page']) && !empty($param['limit'])) {
			$offset = ($param['page'] - 1) * $param['limit'];
			$sql .= " LIMIT {$offset},{$param['limit']} ";
		}
		$query = DB::query($sql);
		$data = $query->execute()->as_array();
		$total = !empty($data) ? DB::count_last_query() : 0;

		return array($total, $data);
	}

	/**
	 * Get detail of mail situation
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return array Returns detail of mail situation
	 */
	public static function get_detail($param)
	{
		$sql = "
			SELECT id,
				   name,
				   name_en,
				   name_cn,
				   image_url,
				   disable,
				   created,
				   updated,
				   template_count,
				   SUM(jp) AS template_count_jp,
				   SUM(en) AS template_count_en,
				   SUM(cn) AS template_count_cn
			FROM   (SELECT mail_situations.*,
						   IF (language_type = '" . \Config::get('language_type')['jp'] . "', 1, 0) AS jp,
						   IF (language_type = '" . \Config::get('language_type')['en'] . "', 1, 0) AS en,
						   IF (language_type = '" . \Config::get('language_type')['cn'] . "', 1, 0) AS cn
					FROM   mail_situations
						   LEFT JOIN templates
							 ON mail_situations.id = templates.mail_situation_id) templates
			WHERE     disable = 0
			AND 	  id = {$param['id']}
		";
		$query = DB::query($sql);
		$data = $query->execute()->as_array();
		if (empty($data)) {
			static::errorNotExist('id', $param['id']);
		}

		return $data ? $data[0] : array();
	}

	/**
	 * Add and update info for Mail Situation
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return int|bool Returns mail situation id or false if error
	 */
	public static function add_update($param)
	{
		try {
			$id = !empty($param['id']) ? $param['id'] : 0;
			$situation = new self;
			if (!empty($id)) {
				$situation = self::find($id);
				if (empty($situation)) {
					static::errorNotExist('mail_situation_id', $id);
					return false;
				}
			}

			if (!empty($param['name'])) {
				$situation->set('name', $param['name']);
			}
			if (!empty($param['name_en'])) {
				$situation->set('name_en', $param['name_en']);
			}
			if (!empty($param['name_cn'])) {
				$situation->set('name_cn', $param['name_cn']);
			}
			if (isset($param['template_count']) && $param['template_count'] != '') {
				$situation->set('template_count', $param['template_count']);
			}
			if (!empty($param['image_url'])) {
				$situation->set('image_url', $param['image_url']);
			}

			if ($situation->save()) {
				if (empty($situation->id)) {
					$situation->id = self::cached_object($situation)->_original['id'];
				}
				return !empty($situation->id) ? $situation->id : 0;
			}
			return false;
		} catch (\Exception $e) {
			\LogLib::error($e->getMessage());
			return false;
		}
	}

	/**
	 * Disable/enable a mail situation
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return bool Returns result of action
	 */
	public static function disable($param)
	{
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $situation = self::find($id);
            if ($situation) {
                $situation->set('disable', $param['disable']);
                if (!$situation->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('mail_situation_id', $id);
                return false;
            }
        }

        return true;
	}

	/**
	 * Get all Mail Situations
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data
	 * @return array Returns array all Mail Situations
	 */
	public static function get_all($param)
	{
		$sql = "
			SELECT    A.id, (
					  CASE {$param['language_type']}
								WHEN '" . \Config::get('language_type')['jp'] . "' THEN A.name
								WHEN '" . \Config::get('language_type')['en'] . "' THEN A.name_en
								WHEN '" . \Config::get('language_type')['cn'] . "' THEN A.name_cn
					  END) AS name,
					  A.image_url,
					  A.disable,
					  A.created,
					  A.updated,
					  A.template_count    AS template_count_all,
					  COUNT(templates.id) AS template_count
			FROM      mail_situations A
			LEFT JOIN
					  (
							 SELECT templates.*
							 FROM   templates
							 WHERE  language_type = {$param['language_type']}
							 AND    disable = 0) templates
			ON        (
								templates.mail_situation_id = A.id )
			WHERE     A.disable = 0
			GROUP BY  A.id
		";
		$query = DB::query($sql);
		$result = $query->execute()->as_array();
		// only get record has template
        $data = array();
        if ($param['os'] != 'webos') {
            foreach ($result as $item) {
                if ($item['template_count'] != '0') {
                    $data[] = $item;
                }
            }
        } else {
            $data = $result;
        }
        return $data;
	}
}
