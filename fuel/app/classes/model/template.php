<?php

/**
 * Any query for model Template
 *
 * @package Model
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Template extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'list_no',
        'mail_type_id',
        'mail_situation_id',
        'title',
        'subject',
        'content',
        'memo',
        'language_type',
        'is_public',
        'made_by',
        'favorite_count',
        'monthly_used_count',
        'is_angry_saizo',
        'angry_comment',
        'not_sendable',
        'not_editable',
        'background_image',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'templates';

    /**
     * Get list Templates
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns list Templates
     */
    public static function get_list($param)
    {
        $param['user_id'] = !empty($param['user_id']) ? $param['user_id'] : 0;
        $query = DB::select(
            self::$_table_name.'.id',
            self::$_table_name.'.list_no',
            self::$_table_name.'.mail_type_id',
            self::$_table_name.'.mail_situation_id',
            self::$_table_name.'.title',
            self::$_table_name.'.subject',
            self::$_table_name.'.content',
            self::$_table_name.'.memo',
            self::$_table_name.'.language_type',
            self::$_table_name.'.is_public',
            self::$_table_name.'.made_by',
            self::$_table_name.'.favorite_count',
            self::$_table_name.'.monthly_used_count',
            self::$_table_name.'.is_angry_saizo',
            self::$_table_name.'.angry_comment',
            self::$_table_name.'.not_sendable',
            self::$_table_name.'.not_editable',
            self::$_table_name.'.background_image',
            self::$_table_name.'.disable',
            self::$_table_name.'.created',
            self::$_table_name.'.updated',
            DB::expr(
                "
				(CASE language_type
					WHEN '".\Config::get('language_type')['jp']."' THEN mail_types.name
					WHEN '".\Config::get('language_type')['en']."' THEN mail_types.name_en
					WHEN '".\Config::get('language_type')['cn']."' THEN mail_types.name_cn
				END) as mail_type_name
			"
            ),
            DB::expr(
                "
				(CASE language_type
					WHEN '".\Config::get('language_type')['jp']."' THEN mail_situations.name
					WHEN '".\Config::get('language_type')['en']."' THEN mail_situations.name_en
					WHEN '".\Config::get('language_type')['cn']."' THEN mail_situations.name_cn
				END) as mail_situation_name
			"
            ),
            array('users.name', 'username')

        )
            ->from(self::$_table_name)
            ->join('mail_types')
            ->on(self::$_table_name.'.mail_type_id', '=', 'mail_types.id')
            ->join('mail_situations')
            ->on(self::$_table_name.'.mail_situation_id', '=', 'mail_situations.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name.'.made_by', '=', 'users.id');

        if ($param['language_type'] != 'all') {
            $query->where(self::$_table_name.'.language_type', '=', $param['language_type']);
        }
        if (!empty($param['mail_type_id'])) {
            $query->where(self::$_table_name.'.mail_type_id', '=', $param['mail_type_id']);
        }
        if (!empty($param['mail_situation_id'])) {
            $query->where(self::$_table_name.'.mail_situation_id', '=', $param['mail_situation_id']);
        }
        if (!empty($param['username'])) {
            $query->where('users.name', 'LIKE', "%{$param['username']}%");
        }
        if (!empty($param['user_id'])) {
            $query->where('users.id', '=', $param['user_id']);
        }
        if (!empty($param['title'])) {
            $query->where(self::$_table_name.'.title', 'LIKE', "%{$param['title']}%");
        }
        if (isset($param['is_public']) && $param['is_public'] != '') {
            $query->where(self::$_table_name.'.is_public', '=', $param['is_public']);
        }
        if (isset($param['not_sendable']) && $param['not_sendable'] != '') {
            $query->where(self::$_table_name.'.not_sendable', '=', $param['not_sendable']);
        }
        if (isset($param['not_editable']) && $param['not_editable'] != '') {
            $query->where(self::$_table_name.'.not_editable', '=', $param['not_editable']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name.'.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name.'.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Get detail of Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns detail of Template
     */
    public static function get_detail($param)
    {
        $param['user_id'] = !empty($param['user_id']) ? $param['user_id'] : 0;
        $query = DB::select(
            self::$_table_name.'.id',
            self::$_table_name.'.list_no',
            self::$_table_name.'.mail_type_id',
            self::$_table_name.'.mail_situation_id',
            self::$_table_name.'.title',
            self::$_table_name.'.subject',
            self::$_table_name.'.content',
            self::$_table_name.'.memo',
            self::$_table_name.'.language_type',
            self::$_table_name.'.is_public',
            self::$_table_name.'.made_by',
            self::$_table_name.'.favorite_count',
            self::$_table_name.'.monthly_used_count',
            self::$_table_name.'.is_angry_saizo',
            self::$_table_name.'.angry_comment',
            self::$_table_name.'.not_sendable',
            self::$_table_name.'.not_editable',
            self::$_table_name.'.background_image',
            self::$_table_name.'.disable',
            self::$_table_name.'.created',
            self::$_table_name.'.updated',
            DB::expr(
                "
				(CASE language_type
					WHEN '".\Config::get('language_type')['jp']."' THEN mail_types.name
					WHEN '".\Config::get('language_type')['en']."' THEN mail_types.name_en
					WHEN '".\Config::get('language_type')['cn']."' THEN mail_types.name_cn
				END) as mail_type_name
			"
            ),
            DB::expr(
                "
				(CASE language_type
					WHEN '".\Config::get('language_type')['jp']."' THEN mail_situations.name
					WHEN '".\Config::get('language_type')['en']."' THEN mail_situations.name_en
					WHEN '".\Config::get('language_type')['cn']."' THEN mail_situations.name_cn
				END) as mail_situation_name
			"
            ),
            array('users.name', 'username_made_by'),
            DB::expr(
                "
				IF (ISNULL(favorites.id), 0, 1) AS is_favorite
			"
            )
        )
            ->from(self::$_table_name)
            ->join('mail_types')
            ->on(self::$_table_name.'.mail_type_id', '=', 'mail_types.id')
            ->join('mail_situations')
            ->on(self::$_table_name.'.mail_situation_id', '=', 'mail_situations.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name.'.made_by', '=', 'users.id')
            ->join(
                DB::expr(
                    "(
				SELECT *
				FROM   template_favorites
				WHERE  disable = 0
					   AND user_id = {$param['user_id']}
					   AND template_id = {$param['id']}
			) favorites"
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'favorites.template_id')
            ->where(self::$_table_name.'.id', '=', $param['id']);
        $data = $query->execute()->as_array();
        return $data ? $data[0] : array();
    }

    /**
     * Add and update info for Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Returns template id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $template = new self;
        if (!empty($id)) {
            $template = self::find($id);
            if (empty($template)) {
                static::errorNotExist('template_id', $id);
                return false;
            }
        } else {
            $param['content'] .= \Config::get(\Config::get('default_signature_setting')[$param['language_type']]);
        }

        if (!empty($param['language_type'])) {
            $template->set('language_type', $param['language_type']);
        }
        if (isset($param['list_no']) && $param['list_no'] != '') {
            $template->set('list_no', $param['list_no']);
        }
        if (!empty($param['mail_type_id'])) {
            $template->set('mail_type_id', $param['mail_type_id']);
        }
        if (!empty($param['mail_situation_id'])) {
            $template->set('mail_situation_id', $param['mail_situation_id']);
        }
        if (!empty($param['title'])) {
            $template->set('title', $param['title']);
        }
        if (!empty($param['subject'])) {
            $template->set('subject', $param['subject']);
        }
        if (!empty($param['content'])) {
            $template->set('content', $param['content']);
        }
        if (!empty($param['memo'])) {
            $template->set('memo', $param['memo']);
        }
        if (isset($param['is_public']) && $param['is_public'] != '') {
            $template->set('is_public', $param['is_public']);
        }
        if (isset($param['not_sendable']) && $param['not_sendable'] != '') {
            $template->set('not_sendable', $param['not_sendable']);
        }
        if (isset($param['not_editable']) && $param['not_editable'] != '') {
            $template->set('not_editable', $param['not_editable']);
        }
        if (!empty($param['made_by'])) {
            $template->set('made_by', $param['made_by']);
        }
        if (isset($param['favorite_count']) && $param['favorite_count'] != '') {
            $template->set('favorite_count', $param['favorite_count']);
        }
        if (isset($param['monthly_used_count']) && $param['monthly_used_count'] != '') {
            $template->set('monthly_used_count', $param['monthly_used_count']);
        }
        if (isset($param['is_angry_saizo']) && $param['is_angry_saizo'] != '') {
            $template->set('is_angry_saizo', $param['is_angry_saizo']);
        }
        if (!empty($param['angry_comment'])) {
            $template->set('angry_comment', $param['angry_comment']);
        }

        if (!empty($_FILES)) {
            $uploadResult = \Lib\Util::uploadImage();
            if (isset($uploadResult['body']['background_image'])) {
                $template->set('background_image', $uploadResult['body']['background_image']);
            }
        }
        if ($template->save()) {
            if (empty($template->id)) {
                $template->id = self::cached_object($template)->_original['id'];
            }
            return !empty($template->id) ? $template->id : 0;
        }
        return false;
    }

    /**
     * Disable/enable a Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $template = self::find($id);
            if ($template) {
                $template->set('disable', $param['disable']);
                if (!$template->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('template_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Get all Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Returns array all Template
     */
    public static function get_all($param)
    {
        $param['user_id'] = !empty($param['user_id']) ? $param['user_id'] : 0;
        $query = DB::select(
            self::$_table_name.'.id',
            self::$_table_name.'.list_no',
            self::$_table_name.'.mail_type_id',
            self::$_table_name.'.mail_situation_id',
            self::$_table_name.'.title',
            self::$_table_name.'.subject',
            self::$_table_name.'.content',
            self::$_table_name.'.memo',
            self::$_table_name.'.language_type',
            self::$_table_name.'.is_public',
            self::$_table_name.'.made_by',
            self::$_table_name.'.favorite_count',
            self::$_table_name.'.monthly_used_count',
            self::$_table_name.'.is_angry_saizo',
            self::$_table_name.'.angry_comment',
            self::$_table_name.'.not_sendable',
            self::$_table_name.'.not_editable',
            self::$_table_name.'.background_image',
            self::$_table_name.'.disable',
            self::$_table_name.'.created',
            self::$_table_name.'.updated',
            DB::expr(
                "
				(CASE language_type
					WHEN '".\Config::get('language_type')['jp']."' THEN mail_types.name
					WHEN '".\Config::get('language_type')['en']."' THEN mail_types.name_en
					WHEN '".\Config::get('language_type')['cn']."' THEN mail_types.name_cn
				END) as mail_type_name
			"
            ),
            DB::expr(
                "
				(CASE language_type
					WHEN '".\Config::get('language_type')['jp']."' THEN mail_situations.name
					WHEN '".\Config::get('language_type')['en']."' THEN mail_situations.name_en
					WHEN '".\Config::get('language_type')['cn']."' THEN mail_situations.name_cn
				END) as mail_situation_name
			"
            ),
            array('users.name', 'username_made_by'),
            DB::expr(
                "
				IF (ISNULL(favorites.id), 0, 1) AS is_favorite
			"
            )
        )
            ->from(self::$_table_name)
            ->join('mail_types')
            ->on(self::$_table_name.'.mail_type_id', '=', 'mail_types.id')
            ->join('mail_situations')
            ->on(self::$_table_name.'.mail_situation_id', '=', 'mail_situations.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name.'.made_by', '=', 'users.id')
            ->join(
                DB::expr(
                    "(
				SELECT *
				FROM   template_favorites
				WHERE  disable = 0
					   AND user_id = {$param['user_id']}
			) favorites"
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'favorites.template_id')
            ->where(self::$_table_name.'.language_type', '=', $param['language_type'])
            ->where(self::$_table_name.'.disable', '=', '0');
        if (!empty($param['mail_type_id'])) {
            $query->where('mail_type_id', $param['mail_type_id']);
        }
        if (!empty($param['mail_situation_id'])) {
            $query->where('mail_situation_id', $param['mail_situation_id']);
            $query->order_by(self::$_table_name.'.list_no', 'ASC');
        } else {
            $query->order_by(self::$_table_name.'.id', 'ASC');
        }

        $data = $query->execute()->as_array();
        if ($data) {
            $signature = Model_Signature::get_detail(
                array(
                    'user_id'       => $param['user_id'],
                    'language_type' => $param['language_type'],
                )
            );
            $content = $signature ? $signature['content'] : \Config::get(
                \Config::get('default_signature_setting')[$param['language_type']]
            );
            foreach ($data as &$item) {
                // not editable, not add signature
                if ($item['not_editable'] == '0') $item['content'] .= $content;
            }
        }
        return $data;
    }

    /**
     * Public/private a Template
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function is_public($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $template = self::find($id);
            if ($template) {
                $template->set('is_public', $param['is_public']);
                if (!$template->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('template_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * check sendable status
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function not_sendable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $template = self::find($id);
            if ($template) {
                $template->set('not_sendable', $param['not_sendable']);
                if (!$template->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('template_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * check editable status
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Returns result of action
     */
    public static function not_editable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $template = self::find($id);
            if ($template) {
                $template->set('not_editable', $param['not_editable']);
                if (!$template->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('template_id', $id);
                return false;
            }
        }

        return true;
    }
    
    /**
     * Get monthly used templates report.
     *
     * @author thailh
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_monthly_used_report($param)
    {
        $query = DB::select(
                self::$_table_name . '.id',
                self::$_table_name . '.title',             
                self::$_table_name . '.monthly_used_count'
            )
            ->from(self::$_table_name) 
            ->where(self::$_table_name . '.disable', '=', '0');
        $query->order_by('monthly_used_count', 'DESC');
        $query->limit(10)->offset(0);
        $data = $query->execute()->as_array();
        return $data;
    }
    
    /**
     * Get DAU (daily active user) report.
     *
     * @author thailh
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_dau_report($param)
    {
        if (empty($param['date_from'])) {
            $param['date_from'] = date('Y-m-1');
        }
        if (empty($param['date_to'])) {
            $days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($param['date_from'])), date('Y', strtotime($param['date_from'])));
            $param['date_to'] = date('Y-m-d', strtotime($param['date_from']) + ($days - 1)*24*60*60);
        }
        $param['date_from'] = self::date_from_val($param['date_from']);
        $param['date_to'] = self::date_to_val($param['date_to']);
        if ($param['date_to'] > time()) {
            $param['date_to'] = self::date_to_val(date('Y-m-d', time()));
        }
        $count_user_templates = Lib\Arr::key_value(          
            DB::select(
                DB::expr("FROM_UNIXTIME(created, '%Y-%m-%d') AS day"),
                DB::expr("COUNT(user_id) AS count_user_templates")
            )
            ->from('user_templates')
            ->where('disable', '0')
            ->where('created', '>=', $param['date_from'])
            ->where('created', '<=', $param['date_to'])
            ->group_by('day')
            ->execute()
            ->as_array(), 
            'day',
            'count_user_templates'
        );         
         
        $count_template_favorites = Lib\Arr::key_value(  
            DB::select(
                DB::expr("FROM_UNIXTIME(created, '%Y-%m-%d') AS day"),
                DB::expr("COUNT(user_id) AS count_template_favorites")
            )
            ->from('template_favorites')
            ->where('disable', '0')
            ->where('created', '>=', $param['date_from'])
            ->where('created', '<=', $param['date_to'])
            ->group_by('day')
            ->execute()
            ->as_array(),
            'day',
            'count_template_favorites'
        );
        
        $count_template_open_logs = Lib\Arr::key_value(
            DB::select(
                'day',
                DB::expr("COUNT(user_id) AS count_template_open_logs")
            )
            ->from(DB::expr("(                      
                SELECT FROM_UNIXTIME(created, '%Y-%m-%d') AS day, user_id
                FROM template_open_logs
                WHERE disable = 0
                AND created >= {$param['date_from']}
                AND created <= {$param['date_to']}
                GROUP BY day, user_id                       
            ) AS template_open_logs"))
            ->group_by('day')
            ->execute()
            ->as_array(),
            'day',
            'count_template_open_logs'
        );        
         
        $count_template_send_logs = Lib\Arr::key_value(
            DB::select(
                'day',
                DB::expr("COUNT(user_id) AS count_template_send_logs")
            )
            ->from(DB::expr("(                      
                SELECT FROM_UNIXTIME(created, '%Y-%m-%d') AS day, user_id
                FROM template_send_logs
                WHERE disable = 0
                AND created >= {$param['date_from']}
                AND created <= {$param['date_to']}
                GROUP BY day, user_id                       
            ) AS template_send_logs"))
            ->group_by('day')
            ->execute()
            ->as_array(),
            'day',
            'count_template_send_logs'
        );        
               
        $result = array();
        for ($day = $param['date_from']; $day <= $param['date_to']; $day += 24*60*60) {
            $ymd = date('Y-m-d', $day);
            $item = array(
                'date' => $ymd,
                'count_user_templates' => 0,
                'count_template_favorites' => 0,
                'count_template_open_logs' => 0,
                'count_template_send_logs' => 0,
            );
            $item['count_user_templates'] =  isset($count_user_templates[$ymd]) ? $count_user_templates[$ymd] : 0;
            $item['count_template_favorites'] =  isset($count_template_favorites[$ymd]) ? $count_template_favorites[$ymd] : 0;
            $item['count_template_open_logs'] =  isset($count_template_open_logs[$ymd]) ? $count_template_open_logs[$ymd] : 0;
            $item['count_template_send_logs'] =  isset($count_template_send_logs[$ymd]) ? $count_template_send_logs[$ymd] : 0;
            $result[] = $item;
        }
        return $result; 
    }
    
}
