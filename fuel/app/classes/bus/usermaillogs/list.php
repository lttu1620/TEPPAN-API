<?php

namespace Bus;

/**
 * Get list user mail logs
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserMailLogs_List extends BusAbstract
{
    /** @var array $_length Length of fields */
	protected $_length = array(
		'title'          => array(0, 255),
		'name'           => array(0, 64),
		'mail'           => array(0, 255),
		'template_title' => array(0, 128)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'page',
		'limit'
	);

    /** @var array $_email_format field email */
	protected $_email_format = array(
		'mail'
	);

	/**
	 * Call function get_list() from model user mail logs
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_User_Mail_Log::get_list($data);
			return $this->result(\Model_User_Mail_Log::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
