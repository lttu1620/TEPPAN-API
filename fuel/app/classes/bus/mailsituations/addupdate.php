<?php

namespace Bus;

/**
 * Add and update info for Mail Situation
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class MailSituations_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
	protected $_length = array(
		'id'      => array(1, 11),
		'name'    => array(0, 128),
		'name_en' => array(0, 128),
		'name_cn' => array(0, 128)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'id'
	);

    /** @var array $_default_value field default */
	protected $_default_value = array(
		'template_count' => '0'
	);

	/**
	 * Call function add_update() from model Mail Situation
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Mail_Situation::add_update($data);
			return $this->result(\Model_Mail_Situation::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
