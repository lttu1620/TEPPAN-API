<?php

namespace Bus;

/**
 * Get list Mail Situation
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class MailSituations_List extends BusAbstract
{
    /** @var array $_length Length of fields */
	protected $_length = array(
		'name'    => array(0, 128),
		'name_en' => array(0, 128),
		'name_cn' => array(0, 128),
		'disable' => 1
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'page',
		'limit',
		'disable'
	);

	/**
	 * Call function get_list() from model Mail Situation
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Mail_Situation::get_list($data);
			return $this->result(\Model_Mail_Situation::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
