<?php

namespace Bus;

/**
 * Get all Mail Situation
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class MailSituations_All extends BusAbstract
{
    /** @var array $_default_value field default */
	protected $_default_value = array(
		'language_type' => '1'
	);

	/**
	 * Call function get_all() from model Mail Situation
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Mail_Situation::get_all($data);
			return $this->result(\Model_Mail_Situation::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
