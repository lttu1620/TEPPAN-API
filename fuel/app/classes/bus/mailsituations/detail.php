<?php

namespace Bus;

/**
 * Get detail Mail Situation
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class MailSituations_Detail extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'id'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'id' => array(1, 11)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'id'
	);

	/**
	 * Get detail mail situation by id
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Mail_Situation::get_detail($data);
			return $this->result(\Model_Mail_Situation::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
