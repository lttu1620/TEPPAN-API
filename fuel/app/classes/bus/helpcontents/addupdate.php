<?php

namespace Bus;

/**
 * Add or update info for help content
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class HelpContents_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'          => array(1, 11),
        'title'       => array(1, 40),
        'description' => array(0, 255),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Call function add_update() from model Help Content
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Help_Content::add_update($data);
            return $this->result(\Model_Help_Content::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
