<?php

namespace Bus;

/**
 * Get list help content
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class HelpContents_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'title'   => array(1, 40),
        'disable' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
        'page',
        'limit'
    );

    /**
     * Call function get_list() from model Help Content
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Help_Content::get_list($data);
            return $this->result(\Model_Help_Content::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
