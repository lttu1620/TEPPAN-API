<?php

namespace Bus;

/**
 * Get list campuses
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Campuses_List extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'university_id'
    );

    /**
     * Call function get_list() from model Campus
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campus::get_list($data);
            return $this->result(\Model_Campus::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
