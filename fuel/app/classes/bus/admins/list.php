<?php

namespace Bus;

/**
 * Get list admin
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Admins_List extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1,
        'name' => array(0, 40),
        'login_id' => array(0, 40),
        'password' => array(0, 40)
    );

    /**
     * Call function get_list() from model Admin
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Admin::get_list($data);
            return $this->result(\Model_Admin::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
