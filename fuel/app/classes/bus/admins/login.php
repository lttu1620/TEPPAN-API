<?php

namespace Bus;
use Lib\Util;

/**
 * Get detail admin
 *
 * @package Bus
 * @created 2014-12-23
 * @version 1.0
 * @author <Tuancd>
 * @copyright Oceanize INC
 */
class Admins_Login extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'login_id',
        'password'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'login_id' => array(0, 40),
        'password' => array(0, 40)
    );

    /**
     * Get detail admin by id or login_id
     *
     * @author <Tuancd>
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data) {
        try {            
            if (!empty($data['login_id']) && !empty($data['password'])) {
                $options['where'][] = array(
                    'login_id' => $data['login_id'],
                    'password' => Util::encodePassword($data['password'], $data['login_id']),
                    'disable' => 0
                ); 
                $result = \Model_Admin::find('first', $options);
                if (!empty($result)) {  
                    $result['token'] = \Model_Authenticate::addupdate(array(
                        'user_id' => $result['id'],
                        'regist_type' => 'admin'
                    ));
                    $this->_response = $result;
                } else {
                    \Model_Admin::errorNotExist('admin_infomation', 'information_of_admin');
                    $this->_response = false;
                }
            }
            return $this->result(\Model_Admin::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
