<?php

namespace Bus;

/**
 * Update password for admin
 *
 * @package Bus
 * @created 2015-01-22
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Admins_UpdatePassword extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'password'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'password' => array(6, 100),
    );

    /**
     * Call function update_password() from model Admins
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Admin::update_password($data);
            return $this->result(\Model_Admin::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
