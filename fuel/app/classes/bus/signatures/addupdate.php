<?php

namespace Bus;

/**
 * Add and update info for Signature
 *
 * @package   API
 * @created   2015-02-11
 * @version   1.0
 * @author    Le Tuan Tu
 * @copyright Oceanize INC
 */
class Signatures_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'            => array(1, 11),
        'user_id'       => array(1, 11),
        'language_type' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id',
        'language_type'
    );

    /**
     * Call function add_update() from model Signature
     *
     * @author  Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Signature::add_update($data);
            return $this->result(\Model_Signature::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
