<?php

namespace Bus;

/**
 * Get list Signatures
 *
 * @package Bus
 * @created 2015-02-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Signatures_List extends BusAbstract
{
    /** @var array $_length Length of fields */
	protected $_length = array(
		'user_id'   => array(1, 11)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'user_id'
	);

	/**
	 * Call function get_list() from model Signature
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Signature::get_list($data);
			return $this->result(\Model_Signature::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
