<?php

namespace Bus;

/**
 * Settings_MultiUpdate - Model to operate to Settings's functions
 *
 * @package Bus
 * @created 2014-12-15
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Settings_MultiUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'value',
    );

    /**
     * Call function multi_update() from model Setting
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Setting::multi_update($data);
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
