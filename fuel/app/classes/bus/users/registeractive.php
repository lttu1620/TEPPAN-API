<?php

namespace Bus;

/**
 * Add register active
 *
 * @package Bus
 * @created 2014-12-18
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_RegisterActive extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'token'
	);

    /** @var array $_email_format field email */
	protected $_email_format = array(
		'email'
	);

	/**
	 * Call function register_active() from model User
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_User_Activation::register_active($data);
			return $this->result(\Model_User_Activation::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
