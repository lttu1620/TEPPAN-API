<?php

namespace Bus;

/**
 * Users_Detail - API to get detail of Users
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Users_Login extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'email',
        'password'
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'password' => array(6, 40),
    );

    /**
     * Call function login()
     *
     * @author tuancd
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data) {
        try {
            $result = \Model_User::login($data);
            if (!empty($result)) {  
                $result['token'] = \Model_Authenticate::addupdate(array(
                    'user_id' => $result['id'],
                    'regist_type' => 'user'
                ));
            }
            $this->_response = $result;
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
