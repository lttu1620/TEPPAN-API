<?php

namespace Bus;

/**
 * <Users_Detail - API to get detail of Users>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Users_Detail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id', //user_id
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'            => array(0, 11),
        'language_type' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'language_type'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'language_type' => '1'
    );

    /**
     * Call function get_detail()
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::get_detail($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
