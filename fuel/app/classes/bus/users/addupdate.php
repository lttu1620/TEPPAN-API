<?php

namespace Bus;

/**
 * Users_AddUpdate - API to get list of Users>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Users_AddUpdate extends BusAbstract
{
    /** @var array $_email_format field email */
    protected $_email_format = array(
        'mail',
        'mail_mobile'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'                 => array(0, 11),
        'user_id'            => array(0, 11),
        'name'               => array(0, 64),
        'first_name'         => array(0, 64),
        'last_name'          => array(0, 64),
        'first_name_kana'    => array(0, 64),
        'last_name_kana'     => array(0, 64),
        'image'              => array(0, 255),
        'university_id'      => array(0, 11),
        'campus_id'          => array(0, 11),
        'department_id'      => array(0, 11),
        'university_name'    => array(0, 128),
        'department_name'    => array(0, 128),
        'major'              => array(0, 64),
        'course'             => array(0, 64),
        'grade_id'           => array(0, 4),
        'sex_id'             => 1,
        'grade'              => 1,
        'postcode'           => array(0, 7),
        'prefecture_id'      => array(0, 2),
        'city'               => array(0, 255),
        'address'            => array(0, 255),
        'tel'                => array(0, 32),
        'tel_mobile'         => array(0, 32),
        'mail'               => array(0, 255),
        'mail_mobile'        => array(0, 255),
        'device_id'          => array(0, 255),
        'high_graduate_year' => 4,
        'univ_graduate_year' => 4
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id',
        'university_id',
        'department_id',
        'campus_id',
        'grade_id',
        'sex_id',
        'postcode',
        'prefecture_id',
        'high_graduate_year',
        'univ_graduate_year'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'language_type' => '1',
        'name'          => '',
        'grade'         => '1'
    );

    /**
     * Call function add_update() from model User
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::add_update($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
