<?php

namespace Bus;

/**
 * <Users_Disable - API to disable or enable a Users>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Users_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'disable',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
    );

    /**
     * Call function disable()
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User::disable($data);
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
