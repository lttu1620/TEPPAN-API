<?php

namespace Bus;

/**
 * Get number user
 *
 * @package Bus
 * @created 2015-06-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_Number extends BusAbstract
{
    protected $_required = array(
        'date',
        'authkey',
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'from_date',
        'to_date'
    );

    /**
     * Call function get_number() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = array(
                'success' => $this->result(\Model_User::error()),
                'number'  => \Model_User::get_number($data)
            );
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
