<?php

namespace Bus;

/**
 * Add guest login
 *
 * @package Bus
 * @created 2015-02-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_GuestLogin extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array( 
        'device_id' => array(0, 255)
    );

    /**
     * Call function guest_login() from model User Guest Id
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $result['id'] = \Model_User_Guest_Id::guest_login($data);
            $result['token'] = \Model_Authenticate::addupdate(array(
                'user_id' => $result['id'],
                'regist_type' => 'guest'
            ));
            $this->_response = $result;
            return $this->result(\Model_User_Guest_Id::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
