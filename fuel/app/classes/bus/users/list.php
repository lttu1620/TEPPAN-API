<?php

namespace Bus;

/**
 * Users_List - API to get list of Users
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Users_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'          => array(0, 40),
        'nickname'      => array(0, 40),
        'university_id' => array(0, 11),
        'department_id' => array(0, 11),
        'campus_id'     => array(0, 11),
        'sex_id'        => 1,
        'language_type' => 1,
        'disable' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'university_id',
        'department_id',
        'campus_id',
        'sex_id',
        'language_type',
        'disable',
        'page',
        'limit',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'language_type' => '1'
    );

    /**
     * Call function get_list()
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::get_list($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
