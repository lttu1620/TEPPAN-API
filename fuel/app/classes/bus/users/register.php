<?php

namespace Bus;

/**
 * Register user
 *
 * @package Bus
 * @created 2015-01-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_Register extends BusAbstract
{
    /** @var array $_default_value field default */
    protected $_default_value = array(
        'name'          => '',
        'grade'         => '1'
    );

    /**
     * Call function register() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::register($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
