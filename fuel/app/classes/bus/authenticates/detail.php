<?php

namespace Bus;

/**
 * Get detail authenticates
 *
 * @package Bus
 * @created 2014-12-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Authenticates_Detail extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'token',
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'token' => array(0, 40)
	);

	/**
	 * Get detail authenticates by token
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Authenticate::get_detail($data);
			return $this->result(\Model_Authenticate::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
