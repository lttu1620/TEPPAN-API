<?php

namespace Bus;

/**
 * Add info for authenticates
 *
 * @package Bus
 * @created 2014-12-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Authenticates_Add extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'user_id',
		'regist_type'
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'user_id'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'user_id' => array(1, 11),
		'regist_type' => array(0, 20)
	);

	/**
	 * Call function add() from model authenticate
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Authenticate::add($data);
			return $this->result(\Model_Authenticate::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
