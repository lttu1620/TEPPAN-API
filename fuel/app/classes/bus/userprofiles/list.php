<?php

namespace Bus;

/**
 * Get list user profile
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
        'password' => array(6, 40),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /**
     * Call function get_list() from model User Profile
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::get_list($data);
            return $this->result(\Model_User_Facebook_Information::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
