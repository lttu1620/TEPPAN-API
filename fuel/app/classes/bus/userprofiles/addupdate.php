<?php

namespace Bus;

/**
 * Add or update info for user profile
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'              => array(0, 11),
        'user_id'         => array(0, 11),
        'name'            => array(0, 64),
        'language_type'   => 1,
        'first_name'      => array(0, 64),
        'last_name'       => array(0, 64),
        'university_name' => array(0, 128),
        'department_name' => array(0, 128),
        'major'           => array(0, 64),
        'course'          => array(0, 64),
        'city'            => array(0, 255),
        'address'         => array(0, 255)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'name'          => '',
        'language_type' => '1'
    );

    /**
     * Call function add_update() from model User Profile
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::add_update($data);
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
