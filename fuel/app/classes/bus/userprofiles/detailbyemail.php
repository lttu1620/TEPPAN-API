<?php

namespace Bus;

/**
 * Get detail user profiles
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_DetailByEmail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'email',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );
    
    /**
     * Get detail user profile by email
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['email'])) {
                $conditions['where'][] = array(
                    'email' => $data['email']
                );
                $this->_response = \Model_User_Profile::find('first', $conditions);
            }
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
