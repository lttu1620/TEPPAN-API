<?php

namespace Bus;

/**
 * change password by user id
 *
 * @package Bus
 * @created 2014-12-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_ChangePassword extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'password'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
        'password' => array(6, 40),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
    );

    /**
     * Call function change_password() from model User Profile
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::change_password($data);
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
