<?php

namespace Bus;

/**
 * Update password by token
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_UpdatePassword extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'token',
        'password',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'password' => array(6, 40),
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'regist_type' => 'forget_password'
    );

    /**
     * Call function update_password() from model User Profile
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::update_password($data);
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
