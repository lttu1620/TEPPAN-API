<?php

namespace Bus;

/**
 * Get detail user profiles
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_Detail extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );

    /**
     * Get detail user profile by id
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['id'])) {
                $id = $data['id'];
                $this->_response = \Model_User_Profile::find($id);
                return $this->result(\Model_User_Profile::error());
            }
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
