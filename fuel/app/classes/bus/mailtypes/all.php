<?php

namespace Bus;

/**
 * Get all Mail Type
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class MailTypes_All extends BusAbstract
{
    /** @var array $_default_value field default */
	protected $_default_value = array(
		'language_type' => '1'
	);

	/**
	 * Call function get_all() from model Mail Type
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Mail_Type::get_all($data);
			return $this->result(\Model_Mail_Type::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
