<?php

namespace Bus;

/**
 * Enable/Disable Mail Type
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class MailTypes_Disable extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'id',
		'disable'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'disable' => 1
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'disable'
	);

	/**
	 * Call function disable() from model Mail Type
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Mail_Type::disable($data);
			return $this->result(\Model_Mail_Type::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
