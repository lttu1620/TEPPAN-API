<?php

namespace Bus;

/**
 * Get list Template Open logs
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplateOpenLogs_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'        => array(0, 64),
        'user_id'     => array(1, 11),
        'template_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'template_id',
        'page',
        'limit'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to'   => 'Y-m-d'
    );

    /**
     * Call function get_list() from model Template Open logs
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template_Open_Log::get_list($data);
            return $this->result(\Model_Template_Open_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
