<?php

namespace Bus;

/**
 * GroupSettings_AddUpdate - Model to operate to GroupSettings's functions
 *
 * @package Bus
 * @created 2014-12-15
 * @updated 2014-12-15
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class GroupSettings_AddUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'group',
        'value'
    );

    /**
     * Call function add_update()
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Group_Setting::add_update($data);
            return $this->result(\Model_Group_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
