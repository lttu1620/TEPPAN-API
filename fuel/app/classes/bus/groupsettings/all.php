<?php

namespace Bus;

/**
 * GroupSettings_All - Model to operate to GroupSettings's functions
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class GroupSettings_All extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'group',
    );

    /**
     * Call function get_all()
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Group_Setting::get_all($data);
            return $this->result(\Model_Group_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
