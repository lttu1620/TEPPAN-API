<?php

namespace Bus;

/**
 * Enable/Disable Template favorites
 *
 * @package Bus
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplateFavorites_Disable extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'          => array(1, 11),
        'template_id' => array(1, 11),
        'user_id'     => array(1, 11),
        'disable'     => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'template_id',
        'user_id',
        'disable',
    );

    /**
     * Call function disable() from model Template Favorite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template_Favorite::disable($data);
            return $this->result(\Model_Template_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
