<?php

namespace Bus;

/**
 * Get list template favorite
 *
 * @package Bus
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplateFavorites_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'        => array(0, 64),
        'mail'        => array(0, 255),
        'template_id' => array(1, 11),
        'user_id'     => array(1, 11),
        'disable'     => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'template_id',
        'user_id',
        'disable',
        'page',
        'limit',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'mail',
    );

    /**
     * Call function get_list() from model News Feed Favorite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template_Favorite::get_list($data);
            return $this->result(\Model_Template_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
