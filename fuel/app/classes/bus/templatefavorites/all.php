<?php

namespace Bus;

/**
 * Get list template favorite for mobile
 *
 * @package Bus
 * @created 2015-02-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplateFavorites_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'page',
        'limit'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'language_type' => '1'
    );

    /**
     * Call function get_all() from model Template Favorite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template_Favorite::get_all($data);
            return $this->result(\Model_Template_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
