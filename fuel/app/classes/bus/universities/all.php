<?php

namespace Bus;

/**
 * Get list universities
 *
 * @package Bus
 * @created 2014-12-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_All extends BusAbstract
{
    /**
     * Call function get_all() from model University
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($param)
    {
        try {
            $this->_response = \Model_University::all($param);
            return $this->result(\Model_University::error());            
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
