<?php

namespace Bus;

/**
 * Add or update info for university
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'    => array(1, 45),
        'kana'    => array(1, 90),
        'disable' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
    );

    /**
     * Call function add_update() from model University
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_University::add_update($data);
            return $this->result(\Model_University::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
