<?php

namespace Bus;

/**
 * Get list university
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'    => array(1, 45),
        'kana'    => array(1, 90),
        'disable' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
        'page',
        'limit'
    );

    /**
     * Call function get_list() from model University
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_University::get_list($data);
            return $this->result(\Model_University::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
