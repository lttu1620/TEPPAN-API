<?php

namespace Bus;

/**
 * Get detail university
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_Detail extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'   => array(1, 11),
        'name' => array(1, 45),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );

    /**
     * Get detail university by id or name
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['id'])) {
                $id = $data['id'];
                $this->_response = \Model_University::find($id);
            } else {
                if (!empty($data['name'])) {
                    $conditions['where'][] = array(
                        'name' => $data['name']
                    );
                    $this->_response = \Model_University::find('first', $conditions);
                }
            }
            return $this->result(\Model_University::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
