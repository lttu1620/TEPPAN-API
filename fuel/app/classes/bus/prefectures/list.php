<?php

namespace Bus;

/**
 * Get list Prefecture
 *
 * @package Bus
 * @created 2015-02-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Prefectures_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name' => array(0, 16)
    );

    /**
     * Call function get_list() from model Prefecture
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Prefecture::get_list($data);
            return $this->result(\Model_Prefecture::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
