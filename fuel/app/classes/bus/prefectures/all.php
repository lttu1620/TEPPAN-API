<?php

namespace Bus;

/**
 * Get list Template Send logs
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Prefectures_All extends BusAbstract
{
    /**
     * Call function get_all() from model Template Send logs
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Prefecture::get_all($data);
            return $this->result(\Model_Prefecture::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
