<?php

namespace Bus;

/**
 * Update info for Prefecture
 *
 * @package Bus
 * @created 2015-02-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Prefectures_Update extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'name'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'   => array(1, 11),
        'name' => array(1, 16)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Call function add_update() from model Prefecture
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Prefecture::set_update($data);
            return $this->result(\Model_Prefecture::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
