<?php

namespace Bus;

/**
 * remove Template Send logs
 *
 * @package Bus
 * @created 2015-02-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplateSendLogs_Remove extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'template_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'     => array(1, 11),
        'template_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'template_id'
    );

    /**
     * Call function remove() from model Template Send Logs
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template_Send_Log::remove($data);
            return $this->result(\Model_Template_Send_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
