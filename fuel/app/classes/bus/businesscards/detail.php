<?php

namespace Bus;

/**
 * Get detail Business Card
 *
 * @package Bus
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class BusinessCards_Detail extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'id'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'id' => array(1, 11)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'id'
	);

	/**
	 * Get detail Business Card by id
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Business_Card::get_detail($data);
			return $this->result(\Model_Business_Card::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
