<?php

namespace Bus;

/**
 * Get list Business Card
 *
 * @package Bus
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class BusinessCards_List extends BusAbstract
{
    /** @var array $_length Length of fields */
	protected $_length = array(
		'user_id' => array(1, 11),
		'company' => array(0, 100),
		'name'    => array(1, 64),
		'phone'   => array(0, 36),
		'email'   => array(0, 255)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'user_id',
		'phone'
	);

    /** @var array $_email_format field email */
	protected $_email_format = array(
		'email'
	);

	/**
	 * Call function get_list() from model Business Card
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Business_Card::get_list($data);
			return $this->result(\Model_Business_Card::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
