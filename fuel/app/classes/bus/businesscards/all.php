<?php

namespace Bus;

/**
 * Get all Business Card
 *
 * @package Bus
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class BusinessCards_All extends BusAbstract
{
	/**
	 * Call function get_all() from model Business Card
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Business_Card::get_all($data);
			return $this->result(\Model_Business_Card::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}
}
