<?php

namespace Bus;

/**
 * Enable/Disable Business Card
 *
 * @package Bus
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class BusinessCards_Disable extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'id',
		'disable'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'disable' => 1
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'disable'
	);

	/**
	 * Call function disable() from model Business Card
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Business_Card::disable($data);
			return $this->result(\Model_Business_Card::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
