<?php

namespace Bus;

/**
 * Add and update info for Business Card
 *
 * @package   API
 * @created   2015-02-10
 * @version   1.0
 * @author    Le Tuan Tu
 * @copyright Oceanize INC
 */
class BusinessCards_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'          => array(1, 11),
        'user_id'     => array(1, 11),
        'image'       => array(0, 255),
        'company'     => array(0, 100),
        'department'  => array(0, 255),
        'name'        => array(1, 64),
        'phone'       => array(0, 36),
        'email'       => array(0, 255),
        'postal_code' => array(0, 7),
        'address'     => array(0, 255)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id',
        'phone'
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email'
    );

    /**
     * Call function add_update() from model Business Card
     *
     * @author  Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Business_Card::add_update($data);
            return $this->result(\Model_Business_Card::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
