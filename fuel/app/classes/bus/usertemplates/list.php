<?php

namespace Bus;

/**
 * Get list Template
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserTemplates_List extends BusAbstract
{
    /** @var array $_length Length of fields */
	protected $_length = array(
		'title'     => array(0, 128),
		'title_en'  => array(0, 128),
		'title_cn'  => array(0, 128),
		'is_public' => 1,
		'disable'   => 1
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'page',
		'limit',
		'is_public',
		'disable'
	);

    /** @var array $_default_value field default */
	protected $_default_value = array(
		'language_type' => '1'
	);

	/**
	 * Call function get_list() from model Template
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_User_Template::get_list($data);
			return $this->result(\Model_User_Template::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
