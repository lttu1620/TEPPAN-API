<?php

namespace Bus;

/**
 * Add and update info for User Template
 *
 * @package   API
 * @created   2015-02-12
 * @version   1.0
 * @author    Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserTemplates_Update extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'                => array(1, 11),
        'user_id'           => array(1, 11),
        'business_card_id'  => array(1, 11),
        'template_id'       => array(1, 11),
        'title'             => array(0, 128)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id',
        'business_card_id',
        'template_id'
    );

    /**
     * Call function add() from model User Template
     *
     * @author  Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Template::set_update($data);
            return $this->result(\Model_User_Template::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
