<?php

namespace Bus;

/**
 * Get all User Template
 *
 * @package Bus
 * @created 2015-02-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserTemplates_All extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'language_type',
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'language_type' => '1'
    );

    /**
     * Call function get_all() from model User Template
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Template::get_all($data);
            return $this->result(\Model_User_Template::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
