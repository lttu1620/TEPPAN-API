<?php

namespace Bus;

use Exception;
use Fuel\Core\Input;
use Fuel\Core\Response;
use Fuel\Core\Config;

/**
 * BusAbstract
 * 
 * @package 	Bus
 * @created 	2014-11-17
 * @version     1.0
 * @author      thailh
 * @copyright   Oceanize INC
 */
abstract class BusAbstract {

    const STATUS_OK = 200;
    const ERROR_CODE_INVALID_JSON = 100;
    const ERROR_CODE_INVALED_PARAMETER = 400;
    const ERROR_CODE_DB_ERROR = 500;
    const ERROR_CODE_AUTH_ERROR = 403;
    const ERROR_CODE_FIELD_REQUIRED = 1000;
    const ERROR_CODE_FIELD_LENGTH_MIN = 1001;
    const ERROR_CODE_FIELD_LENGTH_MAX = 1002;
    const ERROR_CODE_FIELD_LENGTH_EXACT = 1003;
    const ERROR_CODE_FIELD_FORMAT_DATE = 1004;
    const ERROR_CODE_FIELD_FORMAT_EMAIL = 1005;
    const ERROR_CODE_FIELD_FORMAT_URL = 1006;
    const ERROR_CODE_FIELD_NUMERIC_MIN = 1007;
    const ERROR_CODE_FIELD_NUMERIC_MAX = 1008;
    const ERROR_CODE_FIELD_NUMERIC_BETWEEN = 1009;
    const ERROR_CODE_FIELD_NOT_EXIST = 1010;
    const ERROR_CODE_FIELD_DUPLICATE = 1011;
    const ERROR_CODE_FIELD_FORMAT_NUMBER = 1012;
    const ERROR_CODE_DENIED_ERROR = 1100;

    protected $_error_code = array();
    protected $_error_message = array(
        self::ERROR_CODE_INVALID_JSON => 'Invalid json format',
        self::ERROR_CODE_INVALED_PARAMETER => 'Invalid parameter',
        self::ERROR_CODE_DB_ERROR => 'Db exception',
        self::ERROR_CODE_AUTH_ERROR => 'Access token is invalid',
    );
    protected $_error_code_validation = array();
    protected $_error_message_validation = array(
        self::ERROR_CODE_INVALED_PARAMETER => 'Invalid parameters',
        self::ERROR_CODE_AUTH_ERROR => 'Access token is invalid',
        self::ERROR_CODE_DB_ERROR => 'Db exception',
        self::ERROR_CODE_FIELD_REQUIRED => 'The %s is required and must contain a value',
        self::ERROR_CODE_FIELD_LENGTH_MIN => 'The %s has to contain at least %s characters',
        self::ERROR_CODE_FIELD_LENGTH_MAX => 'The %s may not contain more than %s characters',
        self::ERROR_CODE_FIELD_LENGTH_EXACT => 'The field %s must contain exactly %s characters',
        self::ERROR_CODE_FIELD_FORMAT_DATE => 'The % must contain a valid formatted date',
        self::ERROR_CODE_FIELD_FORMAT_EMAIL => 'The %s must contain a valid email address',
        self::ERROR_CODE_FIELD_FORMAT_URL => 'The %s must contain a valid URL',
        self::ERROR_CODE_FIELD_FORMAT_NUMBER => 'The %s must contain a valid number',
        self::ERROR_CODE_FIELD_NUMERIC_MIN => 'The minimum numeric value of :label must be %s',
        self::ERROR_CODE_FIELD_NUMERIC_MAX => 'The maximum numeric value of %s must be %s',
        self::ERROR_CODE_FIELD_NUMERIC_BETWEEN => 'The %s may not contain more than %s characters',
        self::ERROR_CODE_FIELD_NOT_EXIST => 'The %s does not exist',
        self::ERROR_CODE_FIELD_DUPLICATE => 'The %s is duplicate data',
        self::ERROR_CODE_DENIED_ERROR => 'The action have been denied by system',
    );
    protected $_formats = array('json', 'php', 'html', 'xml', 'serialize');
    protected $_input_format = 'post';
    protected $_output_format = 'json';
    protected $_success = null;
    protected $_invalid_parameter;
    protected $_exception = null;
    protected $_default_value = array();
    protected $_required = array();
    protected $_length = array();
    protected $_url_format = array();
    protected $_email_format = array();
    protected $_date_format = array();
    protected $_number_format = array();
    protected $_response = array();
    protected $_has_parameter = true;
    protected static $_instance = null;

     /**
     * Get instance of bus object
     * @return array
     * @author thailh
     */
    public final static function getInstance() 
    {
        if (static::$_instance === null) 
        {
            static::$_instance = new static();
        }
        return static::$_instance;
    }

    /**
     * Add error array
     * @return array
     * @author thailh
     */
    protected function _addErrors($error = array()) 
    {
        if (empty($error)) return false; 
        foreach ($error as $err) 
        {
            $this->_addError($err['code'], $err['field'], $err['value']);
        }
    }
    
    /**
     * Add a error
     * @return array
     * @author thailh
     */
    protected function _addError($code, $field, $value = '') 
    {
        if (isset($this->_error_code_validation[$field]))
        {
            return true;
        }
        if (isset($this->_error_message_validation[$code])) {
            $message = sprintf($this->_error_message_validation[$code], $field, $value);
        } else {
            $message = $value;
        }
        $this->_error_code_validation[] = array(
            'field' => $field,
            'code' => $code,
            'message' => $message
        );
    }

    /**
     * Get validation error
     * @return array
     * @author thailh
     */
    protected function _getError() 
    {
        return $this->_error_code_validation;
    }

    /**
     * Get default value setting
     * @return array
     * @author thailh
     */
    public function getDefaultValue() {
        return $this->_default_value;
    }

    /**
     * implements function get required files setting
     * @author thailh 
     * @returns array The array required field
     */
    public function getRequired() {
        return $this->_required;
    }

    /**
     * implements function get length of fields setting
     * @author thailh 
     * @returns array()
     */
    public function getLength() {
        return $this->_length;
    }

    /**
     * implements function setDefaultValue if empty
     * @param  array $data
     * @return array array with default value
     * @author thailh
     */
    public function setDefaultValue($data) 
    {
        $defaultValue = $this->getDefaultValue();
        if (empty($defaultValue)) 
        {
            return $data;
        }
        foreach ($defaultValue as $field => $value) 
        {
            if (empty($data[$field])) 
            {
                $data[$field] = $value;
            }
        }
        foreach ($data as $field => $value) 
        {
            if (empty($data[$field]) && isset($defaultValue[$field])) 
            {
                $data[$field] = $defaultValue[$field];
            }
        }
        return $data;
    }

    /**
     * Function checkRequired
     * @created 2014-12-10
     * @access  public
     * @author  thailh 
     * @param   array $data
     * @param   array $requiredField
     * @return boolean True if data is valid required, false if invalid
     */
    public function checkRequired($data, $requiredField = null)
    {
        if (!$requiredField)
        {
            $requiredField = $this->getRequired();
        }
        if (!isset($data[0]))
        {
            $data = array($data);
        }
        $ok = true;
        foreach ($data as $dt)
        {
            foreach ($requiredField as $key => $field)
            {  
                if (!isset($dt[$field]) || (isset($dt[$field]) && trim($dt[$field]) == ''))
                {
                    $this->_addError(self::ERROR_CODE_FIELD_REQUIRED, $field);
                    $ok = false;
                }
            }
        }

        return $ok;
    }

    /**
     * Function checkLength
     * @created 2014-12-10
     * @access  public
     * @author  thailh 
     * @param   array $data Input data to check
     * @param   array $length Length config for check length
     * @return  bool True if all field are valid, false if have one of fields invalid
     */
    public function checkLength($data, $lengthOfField = null) 
    {
        if (!$lengthOfField) 
        {
            $lengthOfField = $this->getLength();
        }
        if (!isset($data[0])) 
        {
            $data = array($data);
        }
        $ok = true;
        foreach ($data as $d) 
        { 
            foreach ($lengthOfField as $field => $length) 
            {
                if (isset($d[$field])) 
                {   
                    if (is_array($length)) 
                    {
                        if ((mb_strlen($d[$field]) < intval($length[0])) && !empty($d[$field])) 
                        {
                             $this->_addError(self::ERROR_CODE_FIELD_LENGTH_MIN, $field, $length[0]);
                             $ok = false;
                        }
                        if ((mb_strlen($d[$field]) > intval($length[1])) && !empty($d[$field])) 
                        {
                             $this->_addError(self::ERROR_CODE_FIELD_LENGTH_MAX, $field, $length[1]);
                             $ok = false;
                        }
                    }
                    else
                    {
                        if ((mb_strlen($d[$field]) != $length) && !empty($d[$field])) 
                        {
                             $this->_addError(self::ERROR_CODE_FIELD_LENGTH_MAX, $field, $length);
                             $ok = false;
                        }
                    }
                }                
            }
        }
        return $ok;
    }

    /**
     * set error
     * @param array $data
     * @return Response
     * @author thailh
     */
    private function _error($errorCode) 
    {          
        return $this->getResponse($errorCode);
    }    
    
    /**
     * Do all check, and then call operateDB if data is good
     * @param array $data
     * @return Response
     * @author thailh
     */
    public final function execute($json = null, $moreParam = array()) 
    {
        $data = array();
        \LogLib::info('headers:', __METHOD__, Input::headers());        
        \LogLib::info('user_agent:', __METHOD__, Input::user_agent());
        // if have authToken || require authorize 
        if ((\Lib\Util::authToken() || !in_array(\Uri::string(), \Config::get('unauthorize_url')))
            && \Config::get('authorize') == true)
        {            
            $authDetail = \Model_Authenticate::check_token();
            if (empty($authDetail)) 
            {
                $this->_addError(self::ERROR_CODE_AUTH_ERROR, 'token');
                return $this->getResponse(self::ERROR_CODE_AUTH_ERROR);
            }            
            if ($authDetail['regist_type'] == 'admin') 
            {
                $data['admin_id'] = $authDetail['user_id'];                
            } 
            else
            {
                $data['admin_id'] = 0;
                $data['user_id'] = $authDetail['user_id'];
            }          
        }
        if ($this->_has_parameter) 
        {
            if ($this->_input_format == 'json') 
            {
                if ($json === null) 
                {
                    $json = file_get_contents("php://input");
                }
                $data = \Format::forge($json, 'json')->to_array();
                if (!empty($json) && empty($data)) 
                {
                    return $this->_error(self::ERROR_CODE_INVALID_JSON);
                }
            } 
            elseif ($this->_input_format == 'get' || $this->_input_format == 'post') 
            {
                if ($json === null) 
                {
                    $data = array_merge(Input::param(), $data);                  
                }
            }
            if (!empty($moreParam)) {
                $data = array_merge($data, $moreParam);
            }
            // update output from base on output parameter in data        
            if (!isset($data['output']) || !in_array($data['output'], self::$_formats)) {
                $this->_output_format = 'json';
            }

            $data = $this->setDefaultValue($data);

            // check required
            $checkRequired = $this->checkRequired($data);
            if (!$checkRequired) 
            { 
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }

            // check length
            $checkLength = $this->checkLength($data);
            if (!$checkLength)
            {
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }

            // check url
            $checkUrl = $this->checkUrlFormat($data);
            if (!$checkUrl)
            {
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }

            // check email
            $checkEmail = $this->checkEmailFormat($data);
            if (!$checkEmail)
            {
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }

            // check date
            $checkDate = $this->checkDateFormat($data);
            if (!$checkDate)
            {
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }

            // check number
            $checkNumber = $this->checkNumberFormat($data);
            if (!$checkNumber)
            {
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }

            // check data format
            $checkFormat = $this->checkDataFormat($data);
            if (!$checkFormat) 
            { 
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }
        } 
        else 
        {
            $data = $json;
        }
        
        // API for outsite
        // generate key for test
        // echo hash('md5', Config::get('outsite_secret_key') . \Lib\Util::gmtime(date('Y/m/d H:i:s')));
        // echo '-' . \Lib\Util::gmtime(date('Y/m/d H:i:s')); 
        // exit;
        if (Config::get('outsite_check_security') == true
            && !empty($data['date']) 
            && !empty($data['authkey']))  
        {            
            // check valid param
            if (empty($data['date']) || empty($data['authkey'])) 
            {
                $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, 'date_or_authkey');
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            }
            // check valid date 
            // if request date before n minutes (n = Config::get('outsite_request_minute'))
            if ($data['date'] < \Lib\Util::gmtime(date("Y-m-d H:i:s", strtotime('-' . Config::get('outsite_request_minute', 10) . 'minutes')))) 
            {
                $this->_addError(self::ERROR_CODE_AUTH_ERROR, 'date');
                return $this->getResponse(self::ERROR_CODE_AUTH_ERROR);
            }            
            // check valid key
            // key = md5(secret_key + date)
            if (hash('md5', Config::get('outsite_secret_key') . $data['date']) != $data['authkey']) 
            {   
                $this->_addError(self::ERROR_CODE_AUTH_ERROR, 'authkey');
                return $this->getResponse(self::ERROR_CODE_AUTH_ERROR);
            }
        }
        
        /*
        if ($_FILES && !empty($data['image_fields'])) 
        { 
            $imageFields = !empty($data['image_fields']) ? explode(',', $data['image_fields']) : array();
            $videoFields = !empty($data['video_fields']) ? explode(',', $data['video_fields']) : array();                    
            $hasUploadImage = $hasUploadVideo = false;
            foreach ($_FILES as $field => $info) 
            {
                if (in_array($field, $imageFields)) 
                {
                    $hasUploadImage = true;
                }
                if (in_array($field, $videoFields)) 
                {
                    $hasUploadVideo = true;
                }
            }            
            if (!empty($imageFields) && $hasUploadImage) 
            {  
                \LogLib::info("Start upload image:", __METHOD__, $_FILES);
                $uploadResponse = \Lib\Util::uploadImage($imageFields);  
                \LogLib::info("End upload image:", __METHOD__, $uploadResponse);
            }
            if (!empty($videoFields) && $hasUploadVideo) 
            {
                \LogLib::info("Start upload video:", __METHOD__, $_FILES);
                $uploadResponse = \Lib\Util::uploadVideo($videoFields);
                \LogLib::info("End upload video:", __METHOD__, $uploadResponse);
            }          
            if (!empty($uploadResponse)) 
            {            
                if (!empty($uploadResponse['status']) && $uploadResponse['status'] !== self::STATUS_OK) 
                {
                    return $this->getResponse($uploadResponse['status']);
                }
                if (!empty($uploadResponse['body'])) 
                {
                    $data = array_merge($data, $uploadResponse['body']);
                }
            }
        }  
        * 
        */
        
        // set global setting to config 
        $globalSetting = \Model_Setting::get_all(array('type' => 'global'));
        if (!empty($globalSetting)) 
        {            
            foreach ($globalSetting as $setting) 
            {
                Config::set($setting['name'], $setting['value']);
            }            
        } 
        
        $data['os'] = \Lib\Util::os();
        \LogLib::info('input:', __METHOD__, $data);
        $operateDB = $this->operateDB($data);
        if ($operateDB === false) 
        {
            if ($this->_exception != null) 
            {
                \LogLib::error(sprintf("Exception\n"
                                . " - Message : %s\n"
                                . " - Code : %s\n"
                                . " - File : %s\n"
                                . " - Line : %d\n"
                                . " - Stack trace : \n"
                                . "%s", 
                                $this->_exception->getMessage(), 
                                $this->_exception->getCode(), 
                                $this->_exception->getFile(), 
                                $this->_exception->getLine(), 
                                $this->_exception->getTraceAsString()), 
                __METHOD__, $data);
            }
            if (self::_getError()) {
                return $this->getResponse(self::ERROR_CODE_INVALED_PARAMETER);
            } else {                
                return $this->getResponse(self::ERROR_CODE_DB_ERROR);
            }
        }        
        return $this->getResponse(self::STATUS_OK);
    }

    /**
     * Return format as XML
     * @created June 2, 2014
     * @access  public
     * @author  thailh 
     * @return  string
     */
    public function getXMLResponse() 
    {
        return \Lib\Format::forge($this->_response)->to_xml();
    }

    /**
     * Return format as PHP variable
     * @created June 2, 2014
     * @access  public
     * @author  thailh 
     * @return  string
     */
    public function getPhpResponse() 
    {
        return \Lib\Format::forge($this->_response)->to_php();
    }

    /**
     * Return format as PHP variable
     * @created June 2, 2014
     * @access  public
     * @author  thailh 
     * @return  string
     */
    public function getHtmlResponse() 
    {
        return \Lib\Format::forge($this->_response)->to_html();
    }

    /**
     * Return format as PHP serialize variable
     * @created June 2, 2014
     * @access  public
     * @author  thailh 
     * @return  string
     */
    public function getSerializeResponse() 
    {
        return \Lib\Format::forge($this->_response)->to_serialized();
    }

    /**
     * Return format as PHP serialize variable
     * @created June 2, 2014
     * @access  public
     * @author  thailh 
     * @return  string
     */
    public function getJsonResponse() 
    {
        return \Lib\Format::forge($this->_response)->to_json();
    }

    /**
     * Function getResponse
     * @created 2014-12-10
     * @access  public
     * @author  thailh 
     * @return  Response
     */
    public function getResponse($httpStatus = null) 
    { 
        $response = new Response();
        if (!empty($httpStatus)) 
        {
            $this->_error_code = $httpStatus;
            $response->set_status($httpStatus);
        }         
        if ($httpStatus != self::STATUS_OK) 
        {     
            if ($httpStatus == self::ERROR_CODE_DB_ERROR) 
            {  
                $this->_response = array(
                    'status' => $this->_error_code,
                    'error' => array(
                        $this->_exception->getMessage()
                    )                  
                );
            }
            else
            {
                $this->_response = array(
                    'status' => $this->_error_code,
                    'error' => $this->_getError()                   
                );
            }
        }
        else 
        {   
            $this->_response = array(
                'status' => self::STATUS_OK,
                'body' => $this->_response,                
            );
        }
        switch ($this->_output_format) {
            case 'xml':
                $response->set_header('Content-Type', 'text/xml');
                $function = 'getXmlResponse';
                break;
            case 'php':
                $response->set_header('Content-Type', 'text/plain');
                $function = 'getPhpResponse';
                break;
            case 'html':
                $response->set_header('Content-Type', 'text/html');
                $function = 'getHtmlResponse';
                break;
            case 'serialize':
                $response->set_header('Content-Type', 'text/html');
                $function = 'getSerializeResponse';
                break;
            default:
                $response->set_header('Content-Type', 'application/json');
                $function = 'getJsonResponse';
        }       
        $body = call_user_func(array($this, $function)); 
        $response->body($body);
        return $response;
    }

    /**
     * Get response result
     * @created 2014-12-12
     * @access  public
     * @author  thailh 
     * @param   $error
     * @return  true/false
     */
    public function result($error = array()) 
    {
        if (!empty($error)) {
            $this->_addErrors($error); 
            return false;            
        }
        if ($this->_response !== false)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Check data format, will be override at child class if need
     * @created 2014-12-10
     * @access  public
     * @author  thailh 
     * @param   $data
     * @return  mixed
     */
    public function checkDataFormat($data) 
    {
        return true;
    }

    /**
     * Check url format, will be override at child class if need
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool
     */
    public function checkUrlFormat($data)
    {
        foreach ($this->_url_format as $field) {
            if (!empty($data[$field]) && !filter_var($data[$field], FILTER_VALIDATE_URL)) {
                $this->_addError(self::ERROR_CODE_FIELD_FORMAT_URL, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
            }
        }

        return true;
    }

    /**
     * Check email format, will be override at child class if need
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool
     */
    public function checkEmailFormat($data)
    {
        foreach ($this->_email_format as $field) {
            if (!empty($data[$field]) && !filter_var($data[$field], FILTER_VALIDATE_EMAIL)) {
                $this->_addError(self::ERROR_CODE_FIELD_FORMAT_EMAIL, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
            }
        }

        return true;
    }

    /**
     * Check date format, will be override at child class if need
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool
     */
    public function checkDateFormat($data)
    {
        foreach ($this->_date_format as $field => $value) {
            if (!empty($data[$field])) {
                $dt = \DateTime::createFromFormat($value, $data[$field]);
                if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
                    $this->_addError(self::ERROR_CODE_FIELD_FORMAT_DATE, $field, $data[$field]);
                    $this->_invalid_parameter = $field;
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Check number format, will be override at child class if need
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool
     */
    public function checkNumberFormat($data)
    {
        foreach ($this->_number_format as $field) {

            if (!empty($data[$field]) && !is_numeric($data[$field])) {
                $this->_addError(self::ERROR_CODE_FIELD_FORMAT_NUMBER, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
            }
        }

        return true;
    }

    /**
     * Will be override by child class
     * @created 2014-12-10
     * @access  public
     * @author  thailh 
     * @param   $data
     * @return  boolean True if success, false if error
     */
    public abstract function operateDB($data);
}