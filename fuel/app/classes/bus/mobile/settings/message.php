<?php

namespace Bus;

/**
 * Get list of message setting
 *
 * @package Bus
 * @created 2015-02-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Settings_Message extends BusAbstract
{
    /**
     * Call function get_all()
     *
     * @author tuancd
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $response = \Model_Setting::get_all(array('type' => 'message'));
            $this->_response = $response;
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
