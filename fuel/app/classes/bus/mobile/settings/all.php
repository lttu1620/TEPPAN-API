<?php

namespace Bus;

/**
 * Get list of message setting
 *
 * @package Bus
 * @created 2015-03-23
 * @version 1.0
 * @author Quan
 * @copyright Oceanize INC
 */
class Mobile_Settings_All extends BusAbstract
{
    /**
     * Call function get_all()
     *
     * @author QUan
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $response = \Model_Setting::get_all();
            $this->_response = $response;
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
