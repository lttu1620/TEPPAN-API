<?php

namespace Bus;

/**
 * Mobile_UserSettings_Update - API to Update of UserSettings on mobile
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Mobile_UserSettings_Update extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
    );

    /**
     * Call function mobile_update()
     *
     * @author tuancd
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_User_Setting::mobile_update($data);
            return $this->result(\Model_User_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
