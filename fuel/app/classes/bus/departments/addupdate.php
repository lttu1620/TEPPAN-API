<?php

namespace Bus;

/**
 * Add or update info for department
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Departments_AddUpdate extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'university_id'
    );

    /**
     * Call function add_update() from model Department
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Department::add_update($data);
            return $this->result(\Model_Department::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}