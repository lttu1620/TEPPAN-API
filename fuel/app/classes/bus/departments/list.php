<?php

namespace Bus;

/**
 * Get list department
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Departments_List extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'university_id'
    );

    /**
     * Call function get_list() from model Department
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Department::get_list($data);
            return $this->result(\Model_Department::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
