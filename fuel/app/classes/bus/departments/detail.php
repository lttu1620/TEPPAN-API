<?php

namespace Bus;

/**
 * Get detail department
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Departments_Detail extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'university_id'
    );

    /**
     * Get detail department by id, university id or name
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['id'])) {
                $this->_response = \Model_Department::get_detail($data);
            } else {
                if (!empty($data['university_id'])) {
                    $conditions['where'][] = array(
                        'university_id' => $data['university_id']
                    );
                    $this->_response = \Model_Department::find('all', $conditions);
                    if ($this->_response == null) {
                        $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'university_id', $data['university_id']);
                        return false;
                    }
                } else {
                    if (!empty($data['name'])) {
                        $conditions['where'][] = array(
                            'name' => $data['name']
                        );
                        $this->_response = \Model_Department::find('first', $conditions);
                        if ($this->_response == null) {
                            $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'name', $data['name']);
                            return false;
                        }
                    }
                }
            }
            return $this->result(\Model_Department::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
