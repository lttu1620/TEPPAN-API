<?php

namespace Bus;

/**
 * Add and update info for Template
 *
 * @package   API
 * @created   2015-01-05
 * @version   1.0
 * @author    Le Tuan Tu
 * @copyright Oceanize INC
 */
class Templates_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'                => array(1, 11),
        'list_no'           => array(1, 3),
        'mail_type_id'      => array(1, 11),
        'mail_situation_id' => array(1, 11),
        'title'             => array(0, 128),
        'title_en'          => array(0, 128),
        'title_cn'          => array(0, 128),
        'subject'           => array(0, 128),
        'subject_en'        => array(0, 128),
        'subject_cn'        => array(0, 128),
        'memo'              => array(0, 255),
        'is_public'         => 1,
        'not_sendable'      => 1,
        'not_editable'      => 1,
        'made_by'           => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'mail_type_id',
        'mail_situation_id',
        'is_public',
        'not_sendable',
        'not_editable',
        'made_by',
        'list_no',
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'is_public'          => '0',
        'not_sendable'       => '0',
        'not_editable'       => '0',
        'favorite_count'     => '0',
        'monthly_used_count' => '0',
        'is_angry_saizo'     => '0',
        'list_no'            => '0',
        'language_type'      => '1',
    );

    /**
     * Call function add_update() from model Template
     *
     * @author  Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template::add_update($data);
            return $this->result(\Model_Template::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
