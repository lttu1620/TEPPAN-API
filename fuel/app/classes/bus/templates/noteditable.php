<?php

namespace Bus;

/**
 * check editable status
 *
 * @package Bus
 * @created 2015-08-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Templates_NotEditable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'not_editable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'not_editable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'not_editable'
    );

    /**
     * Call function not_editable() from model Template
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template::not_editable($data);
            return $this->result(\Model_Template::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
