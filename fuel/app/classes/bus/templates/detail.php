<?php

namespace Bus;

/**
 * Get detail Template
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Templates_Detail extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'id',
		'user_id'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'id' => array(1, 11)
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'id'
	);

    /** @var array $_default_value field default */
	protected $_default_value = array(
		'language_type' => '1',
		'user_id' => 0
	);

	/**
	 * Get detail Template by id
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Template::get_detail($data);
			return $this->result(\Model_Template::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
