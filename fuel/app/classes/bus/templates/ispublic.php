<?php

namespace Bus;
use Fuel\Core\Log;
use LogLib\LogLib;

/**
 * Public/Private Template
 *
 * @package Bus
 * @created 2015-01-08
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Templates_IsPublic extends BusAbstract
{
    /** @var array $_required field require */
	protected $_required = array(
		'id',
		'is_public'
	);

    /** @var array $_length Length of fields */
	protected $_length = array(
		'is_public' => 1
	);

    /** @var array $_number_format field number */
	protected $_number_format = array(
		'is_public'
	);

	/**
	 * Call function is_public() from model Template
	 *
	 * @author Le Tuan Tu
	 * @param array $data Input data
	 * @return bool Returns result of operate DB
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_Template::is_public($data);
			return $this->result(\Model_Template::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
