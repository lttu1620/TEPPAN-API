<?php

namespace Bus;

/**
 * check sendable status
 *
 * @package Bus
 * @created 2015-08-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Templates_NotSendable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'not_sendable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'not_sendable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'not_sendable'
    );

    /**
     * Call function not_sendable() from model Template
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Template::not_sendable($data);
            return $this->result(\Model_Template::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
