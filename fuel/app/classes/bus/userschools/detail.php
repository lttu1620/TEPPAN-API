<?php

namespace Bus;

/**
 * Get detail info for User Schools
 *
 * @package Bus
 * @created 2015-03-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserSchools_Detail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id'
    );

    /**
     * Call function get_detail() from model User School
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_School::get_detail($data);
            return $this->result(\Model_User_School::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
