<?php

namespace Bus;

/**
 * Add info for User Schools
 *
 * @package Bus
 * @created 2015-03-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserSchools_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'year',
        'month',
        'day'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
        'year'    => array(1, 4),
        'month'   => array(1, 2),
        'day'     => array(1, 2)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'year',
        'month',
        'day'
    );

    /**
     * Call function add() from model User School
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_School::add($data);
            return $this->result(\Model_User_School::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
