<?php

namespace Bus;

/**
 * Update info for User Schools
 *
 * @package Bus
 * @created 2015-03-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserSchools_Update extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'year',
        'month',
        'day'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
        'delay_1'    => 1,
        'studying_1' => 1,
        'delay_2'    => 1,
        'studying_2' => 1,
        'delay_3'    => 1,
        'studying_3' => 1,
        'delay_4'    => 1,
        'studying_4' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'delay_1',
        'studying_1',
        'delay_2',
        'studying_2',
        'delay_3',
        'studying_3',
        'delay_4',
        'studying_4'
    );

    /**
     * Call function update_school() from model User School
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns result of operate DB
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_School::update_school($data);
            return $this->result(\Model_User_School::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
