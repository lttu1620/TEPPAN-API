<?php

/**
 * <Controller_Mobile_UserSettings- Controller for actions on userSetting on mobile>
 *
 * @package Controller
 * @created 2014-12-19
 * @version 1.0
 * @author <tuancd>
 * @copyright Oceanize INC
 */
class Controller_Mobile_Settings extends \Controller_App
{
    /**
     * Get global setting for mobile
     *
     * @author tuancd
     * @return bool
     */
    public function action_global()
    {
        return \Bus\Mobile_Settings_Global::getInstance()->execute();
    }

    /**
     * Get message setting for mobile
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_message()
    {
        return \Bus\Mobile_Settings_Message::getInstance()->execute();
    }

    /**
     * Get all setting for mobile
     *
     * @author Quan
     * @return bool true on success, otherwise false
     */
    public function action_all()
    {
        return \Bus\Mobile_Settings_All::getInstance()->execute();
    }
}
