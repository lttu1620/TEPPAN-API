<?php

/**
 * <Controller_Mobile_UserSettings- Controller for actions on userSetting on mobile>
 *
 * @package Controller
 * @created 2014-12-19
 * @version 1.0
 * @author <tuancd>
 * @copyright Oceanize INC
 */
class Controller_Mobile_Usersettings extends \Controller_App
{
    /**
     * Get list user setting for mobile
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Mobile_Usersettings_List::getInstance()->execute();
    }

    /**
     * Update user setting for mobile
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_update()
    {
        return \Bus\Mobile_Usersettings_Update::getInstance()->execute();
    }

}
