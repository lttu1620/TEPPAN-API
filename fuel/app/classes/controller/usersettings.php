<?php

/**
 * Controller for actions on UserSettings
 *
 * @package Controller
 * @created 2014-12-12
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_UserSettings extends \Controller_App
{

    /**
     * Get all user setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_all()
    {
        return \Bus\UserSettings_All::getInstance()->execute();
    }

    /**
     * Disable user setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\UserSettings_Disable::getInstance()->execute();
    }

    /**
     * Add, update user setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_addupdate()
    {
        return \Bus\UserSettings_AddUpdate::getInstance()->execute();
    }

    /**
     * multi update user setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_multiupdate()
    {
        return \Bus\UserSettings_MultiUpdate::getInstance()->execute();
    }
}
