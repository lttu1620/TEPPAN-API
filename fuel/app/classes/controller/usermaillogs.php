<?php

/**
 * Controller for actions on user mail logs
 *
 * @package Controller
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserMailLogs extends \Controller_App
{
    /**
     * Get list user mail log
     *
     * @author Le Tuan Tu
     * @return bool
     */
	public function action_list() {
		return \Bus\UserMailLogs_List::getInstance()->execute();
	}

    /**
     * Add user mail log
     *
     * @author Le Tuan Tu
     * @return bool
     */
	public function action_add() {
		return \Bus\UserMailLogs_Add::getInstance()->execute();
	}

}