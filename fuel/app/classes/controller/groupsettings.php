<?php

/**
 * Controller for actions on GroupSetting
 *
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_GroupSettings extends \Controller_App
{
    /**
     * Get all group setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_all()
    {
        return \Bus\GroupSettings_All::getInstance()->execute();
    }

    /**
     * Disable group setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\GroupSettings_Disable::getInstance()->execute();
    }

    /**
     * Add, update group setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_addupdate()
    {
        return \Bus\GroupSettings_AddUpdate::getInstance()->execute();
    }
}
