<?php

/**
 * Controller for actions on University
 *
 * @package Controller
 * @created 2014-11-21
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Upload extends \Controller_Rest 
{
    /**
     * Upload image
     *
     * @author thailh
     * @return bool
     */
    public function action_image() 
    {    
        return $this->response(\Lib\Util::uploadImage());        
    }

    /**
     * Upload video
     *
     * @author thailh
     * @return bool
     */
    public function action_video() 
    {
        return $this->response(\Lib\Util::uploadVideo());        
    }

}
