<?php

/**
 * Controller for actions on Mail Types
 *
 * @package Controller
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_MailTypes extends \Controller_App
{
    /**
     * Add, update mail type
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\MailTypes_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list mail type
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\MailTypes_List::getInstance()->execute();
    }

    /**
     * Disable mail type
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\MailTypes_Disable::getInstance()->execute();
    }

    /**
     * Get detail mail type
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\MailTypes_Detail::getInstance()->execute();
    }

    /**
     * Get all mail type
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\MailTypes_All::getInstance()->execute();
    }
}