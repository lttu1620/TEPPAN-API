<?php

/**
 * Controller for actions on University
 *
 * @package Controller
 * @created 2014-11-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Universities extends \Controller_App
{
    /**
     * Get detail university
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\Universities_Detail::getInstance()->execute();
    }

    /**
     * Get all university
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\Universities_All::getInstance()->execute();
    }

    /**
     * Get list university
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Universities_List::getInstance()->execute();
    }

    /**
     * Disable university
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\Universities_Disable::getInstance()->execute();
    }

    /**
     * Add, update university
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addupdate()
    {
        return \Bus\Universities_AddUpdate::getInstance()->execute();
    }

}