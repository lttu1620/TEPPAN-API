<?php

/**
 * Controller for actions on Template Send logs
 *
 * @package Controller
 * @created 2015-02-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_TemplateSendLogs extends \Controller_App
{
    /**
     * Get list template send log
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\TemplateSendLogs_List::getInstance()->execute();
    }

    /**
     * Add template send log
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_add()
    {
        return \Bus\TemplateSendLogs_Add::getInstance()->execute();
    }

    /**
     * Get using template send log
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_using()
    {
        return \Bus\TemplateSendLogs_Using::getInstance()->execute();
    }

    /**
     * remove template send log
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_remove()
    {
        return \Bus\TemplateSendLogs_Remove::getInstance()->execute();
    }
}