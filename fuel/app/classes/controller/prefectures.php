<?php

/**
 * Controller for actions on Prefecture
 *
 * @package Controller
 * @created 2015-02-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Prefectures extends \Controller_App
{
    /**
     * Get all prefecture
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\Prefectures_All::getInstance()->execute();
    }

    /**
     * Get list prefecture
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Prefectures_List::getInstance()->execute();
    }

    /**
     * Update prefecture
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_update()
    {
        return \Bus\Prefectures_Update::getInstance()->execute();
    }
}