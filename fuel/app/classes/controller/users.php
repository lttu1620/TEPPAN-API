<?php

/**
 * <Controller_Users - Controller for actions on Users>
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Users extends \Controller_App
{
    /**
     * Get list user
     *
     * @author diennvt
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Users_List::getInstance()->execute();
    }

    /**
     * Get detail user
     *
     * @author diennvt
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\Users_Detail::getInstance()->execute();
    }

    /**
     * Disable user
     *
     * @author diennvt
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\Users_Disable::getInstance()->execute();
    }

    /**
     * Add, update user
     *
     * @author diennvt
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\Users_AddUpdate::getInstance()->execute();
    }

    /**
     * Register user
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_register()
    {
        return \Bus\Users_Register::getInstance()->execute();
    }

    /**
     * Get number user
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_numbers()
    {
        return \Bus\Users_Number::getInstance()->execute();
    }
}
