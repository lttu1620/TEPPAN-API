<?php

/**
 * Controller for actions on Department
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Departments extends \Controller_App
{
    /**
     * Get detail department
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\Departments_Detail::getInstance()->execute();
    }

    /**
     * Get list department
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Departments_List::getInstance()->execute();
    }

    /**
     * Disable department
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\Departments_Disable::getInstance()->execute();
    }

    /**
     * Add, update department
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addupdate()
    {
        return \Bus\Departments_AddUpdate::getInstance()->execute();
    }

    /**
     * Get all department
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\Departments_All::getInstance()->execute();
    }
}