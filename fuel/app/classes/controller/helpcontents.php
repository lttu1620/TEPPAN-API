<?php

/**
 * Controller for actions on Help Content
 *
 * @package Controller
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_HelpContents extends \Controller_App
{
    /**
     * Get detail help content
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\HelpContents_Detail::getInstance()->execute();
    }

    /**
     * Get list help content
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\HelpContents_List::getInstance()->execute();
    }

    /**
     * Get all help content
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\HelpContents_All::getInstance()->execute();
    }

    /**
     * Disable help content
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\HelpContents_Disable::getInstance()->execute();
    }

    /**
     * Add, update help content
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\HelpContents_AddUpdate::getInstance()->execute();
    }

}