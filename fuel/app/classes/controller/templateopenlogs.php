<?php

/**
 * Controller for actions on Template Open logs
 *
 * @package Controller
 * @created 2015-02-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_TemplateOpenLogs extends \Controller_App
{
    /**
     * Get list template open log
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\TemplateOpenLogs_List::getInstance()->execute();
    }

    /**
     * Add template open log
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_add()
    {
        return \Bus\TemplateOpenLogs_Add::getInstance()->execute();
    }

    /**
     * Get usually template open log
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_usually()
    {
        return \Bus\TemplateOpenLogs_Usually::getInstance()->execute();
    }

}