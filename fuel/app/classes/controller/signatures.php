<?php

/**
 * Controller for actions on Signature
 *
 * @package Controller
 * @created 2015-02-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Signatures extends \Controller_App
{
    /**
     * Add, update signature
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\Signatures_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list signature
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Signatures_List::getInstance()->execute();
    }

    /**
     * Disable signature
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\Signatures_Disable::getInstance()->execute();
    }

    /**
     * Get detail signature
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\Signatures_Detail::getInstance()->execute();
    }

    /**
     * Get all signature
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\Signatures_All::getInstance()->execute();
    }
}