<?php

/**
 * Controller for actions on Template Favorites
 *
 * @package Controller
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_TemplateFavorites extends \Controller_App
{
    /**
     * Get list template favorite
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\TemplateFavorites_List::getInstance()->execute();
    }

    /**
     * Get all template favorite
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\TemplateFavorites_All::getInstance()->execute();
    }

    /**
     * Disable template favorite
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\TemplateFavorites_Disable::getInstance()->execute();
    }

    /**
     * Add template favorite
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_add()
    {
        return \Bus\TemplateFavorites_Add::getInstance()->execute();
    }

}