<?php

/**
 * Controller for actions on Business Card
 *
 * @package Controller
 * @created 2015-02-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_BusinessCards extends \Controller_App
{
    /**
     * Add, update business card
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\BusinessCards_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list business card
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\BusinessCards_List::getInstance()->execute();
    }

    /**
     * Disable business card
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\BusinessCards_Disable::getInstance()->execute();
    }

    /**
     * Get detail business card
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\BusinessCards_Detail::getInstance()->execute();
    }

    /**
     * Get all business card
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\BusinessCards_All::getInstance()->execute();
    }

}