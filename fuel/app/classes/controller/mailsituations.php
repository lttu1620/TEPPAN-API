<?php

/**
 * Controller for actions on Mail Situation
 *
 * @package Controller
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_MailSituations extends \Controller_App
{
    /**
     * Add, update mail situation
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\MailSituations_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list mail situation
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\MailSituations_List::getInstance()->execute();
    }

    /**
     * Disable mail situation
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\MailSituations_Disable::getInstance()->execute();
    }

    /**
     * Get detail mail situation
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\MailSituations_Detail::getInstance()->execute();
    }

    /**
     * Get all mail situation
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\MailSituations_All::getInstance()->execute();
    }
}