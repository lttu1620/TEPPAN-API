<?php

/**
 * Controller for actions on Settings
 *
 * @package Controller
 * @created 2014-12-12
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Settings extends \Controller_App
{
    /**
     * Get detail setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\Settings_Detail::getInstance()->execute();
    }

    /**
     * Get all setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_all()
    {
        return \Bus\Settings_All::getInstance()->execute();
    }

    /**
     * Get list setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Settings_List::getInstance()->execute();
    }

    /**
     * Disable setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\Settings_Disable::getInstance()->execute();
    }

    /**
     * Add, update setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_addupdate()
    {
        return \Bus\Settings_AddUpdate::getInstance()->execute();
    }

    /**
     * multi update setting
     *
     * @author diennvt
     * @return bool
     */
    public function action_multiupdate()
    {
        return \Bus\Settings_MultiUpdate::getInstance()->execute();
    }
}
