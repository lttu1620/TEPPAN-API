<?php

/**
 * Controller for actions on Templates
 *
 * @package Controller
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Templates extends \Controller_App
{
    /**
     * Add, update template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addUpdate()
    {
        return \Bus\Templates_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\Templates_List::getInstance()->execute();
    }

    /**
     * Disable template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\Templates_Disable::getInstance()->execute();
    }

    /**
     * Get detail template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\Templates_Detail::getInstance()->execute();
    }

    /**
     * Get all template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\Templates_All::getInstance()->execute();
    }

    /**
     * check public status
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_isPublic()
    {
        return \Bus\Templates_IsPublic::getInstance()->execute();
    }

    /**
     * check sendable status
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_notSendable()
    {
        return \Bus\Templates_NotSendable::getInstance()->execute();
    }

    /**
     * check editable status
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_notEditable()
    {
        return \Bus\Templates_NotEditable::getInstance()->execute();
    }
}