<?php

/**
 * Controller for actions on User Templates
 *
 * @package Controller
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserTemplates extends \Controller_App
{
    /**
     * Add user template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_add()
    {
        return \Bus\UserTemplates_Add::getInstance()->execute();
    }

    /**
     * Get list user template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list()
    {
        return \Bus\UserTemplates_List::getInstance()->execute();
    }

    /**
     * Update user template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_update()
    {
        return \Bus\UserTemplates_Update::getInstance()->execute();
    }

    /**
     * Get all user template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_all()
    {
        return \Bus\UserTemplates_All::getInstance()->execute();
    }

    /**
     * Get detail user template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail()
    {
        return \Bus\UserTemplates_Detail::getInstance()->execute();
    }

    /**
     * Disable user template
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_disable()
    {
        return \Bus\UserTemplates_Disable::getInstance()->execute();
    }

}
