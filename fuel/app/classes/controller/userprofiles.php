<?php

/**
 * Controller for actions on User Profile
 *
 * @package Controller
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserProfiles extends \Controller_App
{
    /**
     * Get detail user profile
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detail() {
        return \Bus\UserProfiles_Detail::getInstance()->execute();
    }

    /**
     * Get detail user profile by email
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_detailByEmail() {
        return \Bus\UserProfiles_DetailByEmail::getInstance()->execute();
    }

    /**
     * Get list user profile
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_list() {
        return \Bus\UserProfiles_List::getInstance()->execute();
    }

    /**
     * Add, update user profile
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_addupdate() {
        return \Bus\UserProfiles_AddUpdate::getInstance()->execute();
    }

    /**
     * Update password
     *
     * @author Le Tuan Tu
     * @return bool
     */
    public function action_updatePassword() {
        return \Bus\UserProfiles_UpdatePassword::getInstance()->execute();
    }

    /**
     * change password
     *
     * @author Le Tuan Tu
     * @return bool
     * @example
     */
    public function action_changePassword() {
        return \Bus\UserProfiles_changePassword::getInstance()->execute();
    }
}