<?php
$envConf = array(
	'img_url' => 'http://img.teppan.email/',
	'adm_url' => 'http://teppan.email/',
    'facebook' => array(
        'app_id' => 'AAA',
        'app_secret' => 'AAAAA',
    )
);
if (isset($_SERVER['SERVER_NAME'])) {
    if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php')) {
        include_once (__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php');
        $envConf = array_merge($envConf, $domainConf);
    }
}
return $envConf;