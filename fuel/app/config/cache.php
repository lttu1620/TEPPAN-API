<?php
return array(
    'driver' => 'file',
    'expiration' => null,
    'path' => APPPATH . 'cache/',  
	'key' => array(
		'university_all' => 24*60*60,
		'setting_all' => 24*60*60,
	)
);
