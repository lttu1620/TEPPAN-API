<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;
use LogLib\LogLib;
use Fuel\Core\Package;

/**
 * Master task
 * RUN BATCH: 
 * php oil refine Master
 * FUEL_ENV=test php oil refine Master
 * FUEL_ENV=production php oil refine Master
 * @author thailh
 */
class Master {

    public static function run($env = 'development') {
        try {           
            $sleep = 10*60;
            set_time_limit(0);            
            $dir = DOCROOT;            
            $cmd = array(
                'NewsSitesRss' => "php {$dir}oil refine NewsSitesRss",
                'PushMessage' => "php {$dir}oil refine PushMessage",
            );
            if ($env != 'development')
            {
                if (strncasecmp(PHP_OS, 'WIN', 3) !== 0) 
                {
                    $cmd['NewsSitesRss'] = "FUEL_ENV={$env} nohup php {$dir}oil refine NewsSitesRss &";                           
                    $cmd['PushMessage'] = "FUEL_ENV={$env} nohup php {$dir}oil refine PushMessage &";                            
                }
                else
                {
                    $cmd['NewsSitesRss'] = "FUEL_ENV={$env} php {$dir}oil refine NewsSitesRss &";                           
                    $cmd['PushMessage'] = "FUEL_ENV={$env} php {$dir}oil refine PushMessage &"; 
                }
            }            
            while (true)
            {	
                Cli::write(PHP_EOL . '-> Start import news feed ' . date('Y-m-d H:i'));
                $pid = exec($cmd['NewsSitesRss']);
                Cli::write(PHP_EOL . '-> PID=' . $pid);
                Cli::write(PHP_EOL . '-> End import news feed  ' . date('Y-m-d H:i'));
                               
                Cli::write(PHP_EOL . '-> Start send message ' . date('Y-m-d H:i'));
                $pid = exec($cmd['PushMessage']);
                Cli::write(PHP_EOL . '-> PID=' . $pid);
                Cli::write(PHP_EOL . '-> End send message ' . date('Y-m-d H:i'));
                
                Cli::write(PHP_EOL . '-> Start sleep ' . $sleep . ' seconds ' . date('Y-m-d H:i'));
                sleep($sleep);
            }
        } catch (Exception $ex) {
             \LogLib::error(sprintf("Exception\n"
                            . " - Message : %s\n"
                            . " - Code : %s\n"
                            . " - File : %s\n"
                            . " - Line : %d\n"
                            . " - Stack trace : \n"
                            . "%s", 
                            $ex->getMessage(), 
                            $ex->getCode(), 
                            $ex->getFile(), 
                            $ex->getLine(), 
                            $ex->getTraceAsString()), 
            __METHOD__);
            Cli::write($ex->getMessage());
        }
    }

}
